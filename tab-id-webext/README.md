# `tab-id-webext`

This WebExtension is used in the crawl.
It assigns each tab a UUID and injects it in the page to allow other WebExtension to send data to
the crawler by referencing a tab.

## License

Licensed under the Apache-2.0 license (see LICENSE).

Copyright (c) 2022 Inria
