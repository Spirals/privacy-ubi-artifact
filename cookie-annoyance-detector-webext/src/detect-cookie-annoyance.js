'use strict';

const getTabIdData = async () => {
  let tabUuid;
  let crawlType;

  let intervalId;

  await new Promise((resolve) => {
    intervalId = setInterval(
      async () => {
        tabUuid = document.getElementById("tab-uuid")?.textContent;
        crawlType = document.getElementById("crawl-type")?.textContent;

        if (tabUuid && crawlType) {
          resolve();
        }
      },
      50,
    );
  });

  clearInterval(intervalId);

  return {
    tabUuid,
    crawlType,
  };
};

const buildMatchingSelector = (filterlistRules) => {
  const currentPageHost = new URL(document.location.href).host;

  return filterlistRules
    .filter(({ domains }) => domains.length === 0 || domains.includes(currentPageHost))
    .map(({ selector }) => selector)
    .join(',');
};

const detectCookieAnnoyances = async () => {
  const cosmeticRulesJSON
    = await (await fetch(browser.runtime.getURL('src/assets/cosmetic_rules.json'))).text();
  // console.log(cosmeticRulesJSON);

  const cosmeticRules = JSON.parse(cosmeticRulesJSON);
  console.log(cosmeticRules);

  // Build a selector from the blocklist rules that match the current domain
  const blocklistSelector = buildMatchingSelector(cosmeticRules.blocklistRules);
  console.log(blocklistSelector);

  // Build a selector from the allowlist rules that match the current domain
  const allowlistSelector = buildMatchingSelector(cosmeticRules.allowlistRules);
  console.log(allowlistSelector);

  const cookieAnnoyanceBlocklistElements
    = Array.from(document.querySelectorAll(blocklistSelector));

  // Filter out allowlisted elements
  const cookieAnnoyanceElements = allowlistSelector
    ? cookieAnnoyanceBlocklistElements.filter((el) => !el.matches(allowlistSelector))
    : cookieAnnoyanceBlocklistElements;

  const cookieAnnoyanceCount = cookieAnnoyanceElements.length;

  console.log(
    `${cookieAnnoyanceCount} cookie annoyance element(s) found:`,
    cookieAnnoyanceElements,
  );

  const { tabUuid, crawlType } = await getTabIdData();

  // Send the count of cookie annoyances to the crawl program
  const urlSearchParams = new URLSearchParams();
  urlSearchParams.set('url', document.location.href);
  urlSearchParams.set('tabUuid', tabUuid);
  urlSearchParams.set('crawlType', crawlType);
  urlSearchParams.set('cookieannoyancecount', cookieAnnoyanceCount);
  await fetch('http://127.0.0.1:3000/cookieannoyance', {
    method: 'POST',
    body: urlSearchParams,
  });
};

window.addEventListener('load', detectCookieAnnoyances);
