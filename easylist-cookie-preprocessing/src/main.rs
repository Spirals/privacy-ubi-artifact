use std::env;
use std::fs;
use std::time;

use regex::RegexSet;
use serde::Serialize;

static COOKIE_LIST_URL: &str = "https://secure.fanboy.co.nz/fanboy-cookiemonster.txt";

#[derive(Debug, Clone, Serialize)]
#[serde(rename_all = "camelCase")]
struct HidingCosmeticRules {
    blocklist_rules: Vec<FilterlistRule>,
    allowlist_rules: Vec<FilterlistRule>,
}

#[derive(Debug, Clone)]
enum RuleType {
    ElementHiding,
    ElementHidingEmulation,
    ElementHidingException,
}

#[derive(Debug, Clone, Serialize)]
struct FilterlistRule {
    domains: Vec<String>,
    selector: String,
}

/// Return a timestamp with the number of milliseconds since the UNIX epoch.
fn date_now() -> u128 {
    let now = time::SystemTime::now();
    now.duration_since(time::UNIX_EPOCH)
        .expect("Time went backwards")
        .as_millis()
}

/// Return the matching type of hiding cosmetic rule, if any.
fn get_matching_hiding_cosmetic_rule(line: &str) -> Vec<usize> {
    lazy_static::lazy_static! {
        // https://help.eyeo.com/en/adblockplus/how-to-write-filters#elemhide_basic
        static ref HIDING_COSMETIC_RULE_RE: RegexSet = RegexSet::new(&[
            r"^[^|@]*##.", // Element hiding
            r"^[^|@]*#\?#.", // Element hiding emulation
            r"^[^|@]*#@#." // Element hiding exception
        ]).unwrap();
    }

    HIDING_COSMETIC_RULE_RE
        .matches(line)
        .iter()
        .collect::<Vec<_>>()
}

/// Split the domain and selector parts of a cosmetic rule.
fn parse_rule_parts(line: &str, divider: &str) -> FilterlistRule {
    let mut rule_parts = line.split(divider);

    let domain_part = rule_parts.next().unwrap();
    let domains = match domain_part {
        "" => vec![],
        d => d.split(',').map(|s| s.to_string()).collect::<Vec<_>>(),
    };

    FilterlistRule {
        domains,
        selector: rule_parts.next().unwrap().to_string(),
    }
}

fn main() -> Result<(), ureq::Error> {
    let cookie_list: String = ureq::get(COOKIE_LIST_URL).call()?.into_string()?;

    let timestamp = date_now();
    fs::write(
        format!("fanboy-cookiemonster_{timestamp}.txt"),
        &cookie_list,
    )
    .expect("Could not write the cookie list to a file");

    let cookie_list_lines = cookie_list.lines();

    let cosmetic_rules = cookie_list_lines
        .clone()
        .filter_map(|line| {
            match get_matching_hiding_cosmetic_rule(line)[..] {
                [0] => Some((RuleType::ElementHiding, parse_rule_parts(line, "##"))),
                [1] => Some((
                    RuleType::ElementHidingEmulation,
                    parse_rule_parts(line, "#?#"),
                )),
                [2] => Some((
                    RuleType::ElementHidingException,
                    parse_rule_parts(line, "#@#"),
                )),
                // Even if the regexes are not mutually exclusive, only a single rule should match at once
                _ => None,
            }
        })
        .collect::<Vec<_>>();

    let blocklist_rules = cosmetic_rules
        .iter()
        .filter_map(|(rule_type, filterlist_rule)| match rule_type {
            RuleType::ElementHiding => Some(filterlist_rule.clone()),
            _ => None,
        })
        .collect::<Vec<_>>();
    let blocklist_emulation_rules =
        cosmetic_rules
            .iter()
            .filter_map(|(rule_type, filterlist_rule)| match rule_type {
                RuleType::ElementHidingEmulation => Some(filterlist_rule.clone()),
                _ => None,
            });
    let allowlist_rules = cosmetic_rules
        .iter()
        .filter_map(|(rule_type, filterlist_rule)| match rule_type {
            RuleType::ElementHidingException => Some(filterlist_rule.clone()),
            _ => None,
        })
        .collect::<Vec<_>>();

    println!(
        "{} lines found in the original list",
        cookie_list_lines.count()
    );
    println!("{} hiding cosmetic rules found", cosmetic_rules.len());
    println!("{} blocklist cosmetic rules found", blocklist_rules.len());
    println!(
        "{} blocklist emulation cosmetic rules found (unsupported)",
        blocklist_emulation_rules.count(),
    );
    println!("{} allowlist cosmetic rules found", allowlist_rules.len());

    let cosmetic_rules_json = serde_json::to_string(&HidingCosmeticRules {
        blocklist_rules,
        allowlist_rules,
    })
    .unwrap();

    fs::write(
        env::args()
            .nth(1)
            .expect("Please pass the output JSON filepath as CLI argument"),
        cosmetic_rules_json,
    )
    .expect("Could not write the filtered cookie list to a file");

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_get_matching_hiding_cosmetic_rule() {
        // Cosmetic rules with no domain
        assert_eq!(get_matching_hiding_cosmetic_rule("###test_id"), vec![0]);
        assert_eq!(get_matching_hiding_cosmetic_rule("##.test_class"), vec![0]);

        // Cosmetic rules with a single domain
        assert_eq!(
            get_matching_hiding_cosmetic_rule("example.com###test_id"),
            vec![0],
        );
        assert_eq!(
            get_matching_hiding_cosmetic_rule("example.com##.test_class"),
            vec![0],
        );

        // Cosmetic rule exception with a single domain
        assert_eq!(
            get_matching_hiding_cosmetic_rule("example.com#@##test_id"),
            vec![2],
        );

        // Cosmetic rules with a list of domains
        assert_eq!(
            get_matching_hiding_cosmetic_rule("example.com,example1.com,example2.com###test_id"),
            vec![0],
        );
        assert_eq!(
            get_matching_hiding_cosmetic_rule("example.com,example1.com,example2.com##.test_class"),
            vec![0],
        );

        // Cosmetic rule exception with a list of domains
        assert_eq!(
            get_matching_hiding_cosmetic_rule("example.com,example1.com#@##test_id"),
            vec![2],
        );

        // Invalid rules
        assert_eq!(
            get_matching_hiding_cosmetic_rule("example.com#.test_class"),
            Vec::<usize>::new(),
        );

        // Snippet cosmetic rules (not a *hiding* cosmetic rule)
        assert_eq!(
            get_matching_hiding_cosmetic_rule("#$#snippet"),
            Vec::<usize>::new()
        );
        assert_eq!(
            get_matching_hiding_cosmetic_rule("example.com#$#snippet"),
            Vec::<usize>::new(),
        );

        // Network rules
        assert_eq!(
            get_matching_hiding_cosmetic_rule("/path"),
            Vec::<usize>::new()
        );
        assert_eq!(
            get_matching_hiding_cosmetic_rule("/path?key=value"),
            Vec::<usize>::new()
        );
        assert_eq!(
            get_matching_hiding_cosmetic_rule("||example.com"),
            Vec::<usize>::new()
        );

        // Exception network rules
        assert_eq!(
            get_matching_hiding_cosmetic_rule("@@/path"),
            Vec::<usize>::new()
        );
        assert_eq!(
            get_matching_hiding_cosmetic_rule("@@||example.com"),
            Vec::<usize>::new()
        );
    }
}
