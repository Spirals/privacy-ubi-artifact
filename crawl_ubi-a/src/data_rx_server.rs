use std::collections::HashMap;
use std::convert::Infallible;
use std::sync::Arc;

use hyper::{Body, Method, Request, Response};
use log::{debug, error};

use crate::DataManager;
use crawl_framework::tab_idle_waiter;

macro_rules! err_unwrap_or_log_return {
    ($expr: expr, $err_msg: expr) => {
        match $expr {
            Ok(x) => x,
            Err(err) => {
                error!("{}: {err}", $err_msg);
                return Ok(Response::default());
            }
        }
    };
}

macro_rules! option_unwrap_or_log_return {
    ($expr: expr, $err_msg: expr) => {
        match $expr {
            Some(x) => x,
            None => {
                error!("{}", $err_msg);
                return Ok(Response::default());
            }
        }
    };
}

pub async fn route(
    net_idle_sender: crossbeam_channel::Sender<tab_idle_waiter::TabNetworkIdle>,
    data_manager: Arc<DataManager>,
    req: Request<Body>,
) -> Result<Response<Body>, Infallible> {
    match (req.method(), req.uri().path()) {
        (&Method::POST, "/fixcount") => {
            let body_bytes = err_unwrap_or_log_return!(
                hyper::body::to_bytes(req).await,
                "Could not get bytes from body in the /fixcount endpoint"
            );

            let params = url::form_urlencoded::parse(body_bytes.as_ref())
                .into_owned()
                .collect::<HashMap<String, String>>();
            debug!("Received POST request from ubi-a-webext: {params:?}");

            let json_data = err_unwrap_or_log_return!(
                serde_json::to_value(params.clone()),
                "Could not parse JSON received at /fixcount endpoint"
            );

            let tab_uuid = option_unwrap_or_log_return!(
                params.get("tabUuid"),
                "Could not extract the tabUuid from the POST params"
            );
            let tab_uuid = tab_idle_waiter::TabUuid(tab_uuid.to_string());
            err_unwrap_or_log_return!(
                data_manager.record_data(&tab_uuid, &json_data).await,
                "Could not record data received at /fixcount endpoint in the database"
            );
            Ok(Response::default())
        }

        (&Method::POST, "/networkidle") => {
            let body_bytes = err_unwrap_or_log_return!(
                hyper::body::to_bytes(req).await,
                "Could not get bytes from body in the /networkidle endpoint"
            );

            let params = url::form_urlencoded::parse(body_bytes.as_ref())
                .into_owned()
                .collect::<HashMap<String, String>>();
            debug!("Received POST request from network-idle-webext");

            let json_data = err_unwrap_or_log_return!(
                serde_json::to_value(params.clone()),
                "Could not parse JSON received at /networkidle endpoint"
            );

            let tab_uuid = option_unwrap_or_log_return!(
                params.get("tabUuid"),
                "Could not extract the tabUuid from the POST params"
            );
            let tab_uuid = tab_idle_waiter::TabUuid(tab_uuid.to_string());
            err_unwrap_or_log_return!(
                net_idle_sender.send(tab_idle_waiter::TabNetworkIdle(tab_idle_waiter::TabUuid(
                    tab_uuid.to_string(),
                ))),
                "Could not notify that a tab is now idle"
            );
            err_unwrap_or_log_return!(
                data_manager.record_data(&tab_uuid, &json_data).await,
                "Could not record data received at /networkidle endpoint in the database"
            );
            Ok(Response::default())
        }

        (&Method::POST, "/cookieannoyance") => {
            let body_bytes = err_unwrap_or_log_return!(
                hyper::body::to_bytes(req).await,
                "Could not get bytes from body in the /cookieannoyance endpoint"
            );

            let params = url::form_urlencoded::parse(body_bytes.as_ref())
                .into_owned()
                .collect::<HashMap<String, String>>();
            debug!("Received POST request from cookie-annoyance-detector-webext: {params:?}");

            let json_data = err_unwrap_or_log_return!(
                serde_json::to_value(params.clone()),
                "Could not parse JSON received at /cookieannoyance endpoint"
            );

            let tab_uuid = option_unwrap_or_log_return!(
                params.get("tabUuid"),
                "Could not extract the tabUuid from the POST params"
            );
            let tab_uuid = tab_idle_waiter::TabUuid(tab_uuid.to_string());
            err_unwrap_or_log_return!(
                data_manager.record_data(&tab_uuid, &json_data).await,
                "Could not record data received at /cookieannoyance endpoint in the database"
            );
            Ok(Response::default())
        }
        _ => Ok(Response::default()),
    }
}
