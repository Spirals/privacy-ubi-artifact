use std::path::{Path, PathBuf};
use std::time::{Duration, Instant};

use log::{debug, error, info};
use serde_json::json;
use thiserror::Error;
use url::Url;

use crawl_framework::data_manager::{self, DataManager, PageTiming};
use crawl_framework::tab_idle_waiter::TabIdleWaiter;
use crawl_framework::thirtyfour::extensions::firefox::FirefoxTools;
use crawl_framework::thirtyfour::prelude::*;
use crawl_framework::thirtyfour::session::handle::SessionHandle;
use crawl_framework::thirtyfour::WindowHandle;
use crawl_framework::CrawlError;

use crate::{CrawlTask, CrawlType};
use crate::{
    ANIMATION_SPOTTER_SCREENSHOT_DELAY, INTERACTIVE_TIMEOUT, LOAD_TIMEOUT, POST_LOAD_TIMEOUT,
};

static CHILD_PAGE_COUNT: usize = 4;

#[derive(Debug, Error)]
pub(crate) enum CheckPageError {
    #[error("{0}")]
    CrawlError(#[from] CrawlError),

    #[error("Database error: {0}")]
    DbError(#[from] data_manager::sqlx::Error),

    #[error("Error when attempting to serialize an error: {0:?}")]
    Serialization(serde_json::Error),
}

impl From<crawl_framework::thirtyfour::error::WebDriverError> for CheckPageError {
    fn from(web_driver_error: crawl_framework::thirtyfour::error::WebDriverError) -> Self {
        Self::CrawlError(web_driver_error.into())
    }
}

#[derive(Debug, Clone)]
enum ScreenshotType {
    Main,
    AnimSpotter,
}

impl std::fmt::Display for ScreenshotType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match *self {
            Self::Main => write!(f, "main"),
            Self::AnimSpotter => write!(f, "animspotter"),
        }
    }
}

type CheckPageResult<T> = Result<T, CheckPageError>;

async fn get_page_background_color(
    session: &tokio::sync::Mutex<SessionHandle>,
    window_handle: WindowHandle,
) -> WebDriverResult<String> {
    let session_guard = session.lock().await;
    session_guard
        .switch_to()
        .window(window_handle.clone())
        .await?;

    session_guard
        .execute_script(
            r#"return getComputedStyle(document.body).backgroundColor"#,
            Vec::new(),
        )
        .await?
        .convert()
}

fn get_screenshot_path(
    screenshot_type: &ScreenshotType,
    url: &str,
    crawl_type: &CrawlType,
    output_dir: &Path,
) -> PathBuf {
    // Truncate filenames to the first 200 characters to accommodate ext4 limitations
    let filename = format!(
        "screenshot_{screenshot_type}_{crawl_type}_{}.png",
        crawl_framework::append_timestamp(&crawl_framework::escape_ext4_filepath(url))
    )
    .chars()
    .take(200)
    .collect::<String>();

    output_dir.join(filename)
}

#[allow(clippy::too_many_arguments)]
#[allow(clippy::too_many_lines)]
pub(crate) async fn inspect_page(
    session: &tokio::sync::Mutex<SessionHandle>,
    window_handle: WindowHandle,
    tools: &FirefoxTools,
    tab_idle_waiter: &TabIdleWaiter,
    data_manager: &DataManager,
    crawl_type: &CrawlType,
    crawl_tasks: &tokio::sync::Mutex<Vec<CrawlTask>>,
    crawl_task: &CrawlTask,
    output_dir: &Path,
) -> CheckPageResult<()> {
    use rand::prelude::IteratorRandom;

    let url = &crawl_task.url;

    // Load the page, wait until the `interactive` readyState
    let (start_timer, goto_res) = {
        let session_guard = session.lock().await;
        session_guard
            .switch_to()
            .window(window_handle.clone())
            .await?;

        let start_timer = Instant::now();

        let goto_res = session_guard.get(&url).await;

        (start_timer, goto_res)
    };

    // Record an error if the page could not be loaded
    {
        let session_guard = session.lock().await;
        session_guard
            .switch_to()
            .window(window_handle.clone())
            .await?;

        if let Err(goto_err) = goto_res {
            if let crawl_framework::thirtyfour::error::WebDriverError::CmdError(
                fantoccini::error::CmdError::Standard(ref web_driver_err),
            ) = goto_err
            {
                data_manager
                    .record_error(
                        url,
                        &serde_json::to_string(&web_driver_err)
                            .map_err(CheckPageError::Serialization)?,
                    )
                    .await?;

                // Bail out if the page could not be loaded
                return Err(goto_err.into());
            }
        }
    }

    // Record the tab data and extract children page URLs if the crawl type is js
    let (tab_uuid, current_url) = {
        let session_guard = session.lock().await;
        session_guard
            .switch_to()
            .window(window_handle.clone())
            .await?;

        let tab_uuid = TabIdleWaiter::get_tab_uuid_with_timeout(
            &session_guard,
            &window_handle,
            Duration::from_millis(1000),
        )
        .await?;

        let current_url = session_guard.current_url().await?;

        data_manager
            .record_tab(
                url,
                current_url.as_str(),
                crawl_task.ranking,
                crawl_task.landing_page,
                &tab_uuid,
                &crawl_type.to_string(),
            )
            .await?;

        if crawl_type == &CrawlType::Js && crawl_task.inspect_children_pages {
            let anchor_elements = session_guard.find_elements(By::Tag("a")).await?;

            let mut same_origin_links = vec![];
            for a in anchor_elements {
                if let Ok(Some(href)) = a.get_property("href").await {
                    if let Ok(href_parsed) = Url::parse(&href) {
                        if current_url.origin() == href_parsed.origin()
                            && current_url.path() != href_parsed.path()
                        {
                            same_origin_links.push(href);
                        }
                    }
                }
            }

            let random_child_page_url_sample = same_origin_links
                .iter()
                .choose_multiple(&mut rand::thread_rng(), CHILD_PAGE_COUNT);

            let children_crawl_tasks = random_child_page_url_sample.iter().map(|&url| CrawlTask {
                ranking: crawl_task.ranking,
                url: url.clone(),
                inspect_children_pages: false, // Do not recursively inspect children pages
                landing_page: false,
            });

            let mut crawl_tasks_guard = crawl_tasks.lock().await;
            crawl_tasks_guard.extend(children_crawl_tasks);
        }

        (tab_uuid, current_url.as_str().to_string())
    };

    let interactive_time = start_timer.elapsed();
    info!(
        "[{crawl_type}] Page {url} interactive in {}\u{a0}ms.",
        interactive_time.as_millis(),
    );

    // Record page interactive time
    data_manager
        .record_page_timing(
            &tab_uuid,
            &PageTiming {
                name: "interactive_time".to_string(),
                duration: interactive_time,
            },
        )
        .await?;

    // Wait for the `complete` readyState
    tab_idle_waiter
        .wait_for_complete_state(
            session,
            window_handle.clone(),
            LOAD_TIMEOUT - INTERACTIVE_TIMEOUT,
        )
        .await?;
    let load_time = start_timer.elapsed();
    info!(
        "[{crawl_type}] Page {url} loaded in {}\u{a0}ms.",
        load_time.as_millis()
    );

    let post_load_timer = Instant::now();

    // Record page load time
    data_manager
        .record_page_timing(
            &tab_uuid,
            &PageTiming {
                name: "load_time".to_string(),
                duration: load_time,
            },
        )
        .await?;

    // Scroll the page a bit
    crawl_framework::scroll_page(session, window_handle.clone()).await?;
    info!("[{crawl_type}] Scrolled the page on {url}");

    info!(
        "[{crawl_type}] Waiting for up to {}\u{a0}s after load on {url}",
        POST_LOAD_TIMEOUT.as_secs()
    );

    // Wait for the tab to be network idling
    tab_idle_waiter
        .wait_for_tab_idle_with_timeout(session, &window_handle, POST_LOAD_TIMEOUT)
        .await?;
    info!(
        "[{crawl_type}] Waited for {}\u{a0}ms after load on {url}",
        post_load_timer.elapsed().as_millis(),
    );

    // Record page post-load time
    data_manager
        .record_page_timing(
            &tab_uuid,
            &PageTiming {
                name: "post_load_time".to_string(),
                duration: post_load_timer.elapsed(),
            },
        )
        .await?;
    // Record page total-load time
    data_manager
        .record_page_timing(
            &tab_uuid,
            &PageTiming {
                name: "total_load_time".to_string(),
                duration: start_timer.elapsed(),
            },
        )
        .await?;

    // Save the main screenshot
    let main_screenshot_path =
        get_screenshot_path(&ScreenshotType::Main, &current_url, crawl_type, output_dir);
    crawl_framework::save_page_screenshot(
        session,
        window_handle.clone(),
        tools,
        main_screenshot_path.clone(),
    )
    .await?;
    debug!("[{crawl_type}] Main screenshot taken for {current_url}");

    data_manager
        .record_screenshot_paths(
            &tab_uuid,
            &json!({ "main": main_screenshot_path.to_string_lossy() }),
        )
        .await?;

    tokio::time::sleep(ANIMATION_SPOTTER_SCREENSHOT_DELAY).await;

    // For the js crawl, store a second screenshot, used to spot pages with autoplay animations
    if crawl_type == &CrawlType::Js {
        let anim_spotter_screenshot_path = get_screenshot_path(
            &ScreenshotType::AnimSpotter,
            &current_url,
            crawl_type,
            output_dir,
        );
        crawl_framework::save_page_screenshot(
            session,
            window_handle.clone(),
            tools,
            anim_spotter_screenshot_path.clone(),
        )
        .await?;
        debug!("[{crawl_type}] Animation-spotter screenshot taken for {current_url}");

        data_manager
            .record_screenshot_paths(
                &tab_uuid,
                &json!({ "animSpotter": anim_spotter_screenshot_path.to_string_lossy() }),
            )
            .await?;
    }

    // Extract and store the page body's background color
    let background_color = get_page_background_color(session, window_handle.clone()).await?;
    {
        data_manager
            .record_data(&tab_uuid, &json!({ "backgroundColor": background_color }))
            .await?;
    }
    debug!("[{crawl_type}] Background color {background_color} saved for {url}");

    Ok(())
}
