#![forbid(unsafe_code)]
// Avoid panics
#![deny(
    clippy::unwrap_used,
    clippy::expect_used,
    clippy::panic,
    clippy::indexing_slicing,
    clippy::unwrap_in_result,
    clippy::unreachable,
    clippy::unimplemented
)]
// Correctness
#![warn(
    clippy::pedantic,
    clippy::clone_on_ref_ptr,
    clippy::non_ascii_literal,
    clippy::dbg_macro,
    clippy::map_err_ignore,
    clippy::use_debug,
    clippy::pattern_type_mismatch,
    clippy::map_err_ignore
)]
#![allow(clippy::doc_markdown, clippy::must_use_candidate)]
// Style
#![warn(
    clippy::use_self,
    clippy::useless_let_if_seq,
    clippy::verbose_file_reads
)]

use std::collections::HashMap;
use std::convert::Infallible;
use std::env;
use std::fs::{self, File};
use std::net::SocketAddr;
use std::path::Path;
use std::process::{self, Command};
use std::sync::Arc;
use std::time::Duration;

use anyhow::Context;
use crawl_framework::install_temp_local_webext;
use crawl_framework::thirtyfour::common::capabilities::desiredcapabilities::PageLoadStrategy;
use crawl_framework::thirtyfour::extensions::firefox::FirefoxTools;
use crawl_framework::thirtyfour::prelude::*;
use hyper::service::{make_service_fn, service_fn};
use hyper::{Body, Request, Server};
use log::{debug, error, info};
use serde::Deserialize;
use simplelog::{
    ColorChoice, CombinedLogger, ConfigBuilder, LevelFilter, TermLogger, TerminalMode, WriteLogger,
};
use tempdir::TempDir;

use crawl_framework::data_manager::DataManager;
use crawl_framework::tab_idle_waiter::TabIdleWaiter;

mod data_rx_server;
mod inspect_page;

static MAX_OPEN_TABS: u8 = 1;

static INTERACTIVE_TIMEOUT: Duration = Duration::from_secs(15);
static LOAD_TIMEOUT: Duration = Duration::from_secs(60);
static POST_LOAD_TIMEOUT: Duration = Duration::from_secs(30);
static ANIMATION_SPOTTER_SCREENSHOT_DELAY: Duration = Duration::from_millis(321);

static WINDOW_SIZE: (u32, u32) = (1280, 800);

#[derive(Debug, Clone, PartialEq, Eq, Hash, Deserialize)]
enum CrawlType {
    Js,
    NoJs,
    NoJsUbi,
}

fn r#true() -> bool {
    true
}

#[derive(Debug, Clone, Deserialize)]
struct CrawlTask {
    ranking: u32,
    url: String,
    #[serde(default = "r#true")]
    inspect_children_pages: bool,
    #[serde(default = "r#true")]
    landing_page: bool,
}

impl std::fmt::Display for CrawlType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match *self {
            Self::Js => write!(f, "js"),
            Self::NoJs => write!(f, "nojs"),
            Self::NoJsUbi => write!(f, "nojs_ubi"),
        }
    }
}

struct DriverTools {
    driver: WebDriver,
    tools: FirefoxTools,
}

async fn setup_firefox_driver(
    port: u16,
    page_load_strategy: &PageLoadStrategy,
    headless: bool,
) -> WebDriverResult<DriverTools> {
    let mut caps = DesiredCapabilities::firefox();
    caps.set_page_load_strategy(page_load_strategy.clone())?;

    if headless {
        caps.set_headless()?;
    }

    let driver = WebDriver::new(&format!("http://localhost:{port}"), caps).await?;

    let load_timeout = match *page_load_strategy {
        PageLoadStrategy::Eager => INTERACTIVE_TIMEOUT,
        PageLoadStrategy::Normal => LOAD_TIMEOUT,
        #[allow(clippy::unimplemented)]
        PageLoadStrategy::None => unimplemented!(),
    };
    driver.set_page_load_timeout(load_timeout).await?;

    driver
        .set_window_rect(
            400_u32 * u32::from(port - 4444),
            200_u32 * u32::from(port - 4444),
            WINDOW_SIZE.0,
            WINDOW_SIZE.1,
        )
        .await?;
    info!("Set the window size to {}x{}", WINDOW_SIZE.0, WINDOW_SIZE.1);

    // No mutex is needed around tools as it is only used to take screenshots, and cannot be used
    // to switch the current window/tab
    let tools = FirefoxTools::new(driver.handle.clone());

    Ok(DriverTools { driver, tools })
}

fn spawn_geckodriver(port: u16) -> process::Child {
    #[allow(clippy::unwrap_used)]
    Command::new("./geckodriver")
        .arg("--log")
        .arg("error")
        .arg("-p")
        .arg(port.to_string())
        .spawn()
        .with_context(|| format!("Could not spawn geckodriver on port {port}"))
        .unwrap()
}

fn create_crawl_type_webext_content_script(
    webext_path: &str,
    tmp_dir: &TempDir,
    crawl_type: &CrawlType,
) -> String {
    let content_script = format!("window.CRAWL_TYPE = '{crawl_type}';");

    let mut copy_options = fs_extra::dir::CopyOptions::new();
    copy_options.copy_inside = true;

    #[allow(clippy::unwrap_used)]
    fs_extra::dir::copy(webext_path, tmp_dir, &copy_options)
        .with_context(|| {
            format!(
                "Could not copy the content of {webext_path} to {}",
                tmp_dir.path().to_string_lossy()
            )
        })
        .unwrap();

    #[allow(clippy::unwrap_used)]
    let webext_dirname = Path::new(webext_path)
        .file_name()
        .unwrap()
        .to_string_lossy();
    let tmp_webext_dir = format!("{}/{webext_dirname}", tmp_dir.path().to_string_lossy());
    let path = format!("{tmp_webext_dir}/src/crawl_type.js");
    #[allow(clippy::unwrap_used)]
    fs::write(&path, content_script)
        .with_context(|| format!("Could not write the crawl type content script for {webext_path} ({crawl_type}) at {path}"))
        .unwrap();

    tmp_webext_dir
}

lazy_static::lazy_static! {
    static ref WEB_DRIVER_PORTS: HashMap<CrawlType, u16> = {
        let mut web_driver_ports = HashMap::new();
        web_driver_ports.insert(CrawlType::Js, 4444);
        web_driver_ports.insert(CrawlType::NoJs, 4445);
        web_driver_ports.insert(CrawlType::NoJsUbi, 4446);
        web_driver_ports
    };
}

#[allow(clippy::too_many_lines)]
#[tokio::main(flavor = "current_thread")]
async fn main() -> anyhow::Result<()> {
    const HEADLESS: bool = true;

    let tab_id_webext = "./tab-id-webext";

    let mut browser_webexts = HashMap::new();
    browser_webexts.insert(
        CrawlType::Js,
        vec![
            "./network-idle-webext",
            "./cookie-annoyance-detector-webext",
        ],
    );
    browser_webexts.insert(
        CrawlType::NoJs,
        vec![
            "./network-idle-webext",
            "./cookie-annoyance-detector-webext",
            "./js-blocker-webext",
        ],
    );
    browser_webexts.insert(
        CrawlType::NoJsUbi,
        vec![
            "./network-idle-webext",
            "./cookie-annoyance-detector-webext",
            "./js-blocker-webext",
            "./ubi-a-webext",
        ],
    );

    #[allow(clippy::unwrap_used)]
    CombinedLogger::init(vec![
        TermLogger::new(
            LevelFilter::Debug,
            ConfigBuilder::new()
                .set_time_format_rfc3339()
                .add_filter_allow_str("crawl_framework")
                .add_filter_allow_str("crawl_ubi_a")
                .build(),
            TerminalMode::Mixed,
            ColorChoice::Auto,
        ),
        WriteLogger::new(
            LevelFilter::Debug,
            ConfigBuilder::new()
                .set_time_format_rfc3339()
                .add_filter_allow_str("crawl_framework")
                .add_filter_allow_str("crawl_ubi_a")
                .build(),
            File::create("crawl.log").unwrap(),
        ),
    ])
    .unwrap();

    // Read URLs to crawl
    let crawl_url_path = env::args().nth(1).context(
        "Please pass the CSV input URL list as the first CLI argument; syntax: $ranking,$url",
    )?;
    let mut crawl_url_reader = csv::Reader::from_path(&crawl_url_path)
        .with_context(|| format!("Could not parse the given CSV url list {crawl_url_path}"))?;
    crawl_url_reader.set_headers(csv::StringRecord::from(vec!["ranking", "url"]));
    let crawl_tasks: Vec<CrawlTask> = crawl_url_reader
        .deserialize()
        .map(Result::unwrap)
        .collect::<Vec<_>>()
        .into_iter()
        .rev() // Reverse the iterator as we are going to pop the Vec later
        .collect::<Vec<_>>();
    let crawl_tasks = Arc::new(tokio::sync::Mutex::new(crawl_tasks));

    // These ports may not be free, but if they are not, this will crash early so it is no big deal
    let mut geckodrivers = WEB_DRIVER_PORTS
        .iter()
        .map(|(crawl_type, &web_driver_port)| (crawl_type, spawn_geckodriver(web_driver_port)))
        .collect::<HashMap<_, _>>();

    // Set up browser drivers
    let mut driver_tools = futures::future::join_all(WEB_DRIVER_PORTS.iter().map(
        |(crawl_type, web_driver_port)| async move {
            #[allow(clippy::unwrap_used)]
            let driver_tool =
                setup_firefox_driver(*web_driver_port, &PageLoadStrategy::Eager, HEADLESS)
                    .await
                    .with_context(|| {
                        format!("Could not set up a WebDriver driver for the {crawl_type} browser")
                    })
                    .unwrap();
            (crawl_type, driver_tool)
        },
    ))
    .await
    .into_iter()
    .collect::<HashMap<_, _>>();

    // This HashMap is initialized this way because of RAII on TempDir
    let mut tab_id_tmp_dirs = HashMap::new();
    #[allow(clippy::unwrap_used)]
    tab_id_tmp_dirs.insert(CrawlType::Js, TempDir::new("").unwrap());
    #[allow(clippy::unwrap_used)]
    tab_id_tmp_dirs.insert(CrawlType::NoJs, TempDir::new("").unwrap());
    #[allow(clippy::unwrap_used)]
    tab_id_tmp_dirs.insert(CrawlType::NoJsUbi, TempDir::new("").unwrap());

    // Install appropriate extensions
    #[allow(clippy::unwrap_used)]
    for (crawl_type, _) in WEB_DRIVER_PORTS.iter() {
        let tab_id_tmp_dir = create_crawl_type_webext_content_script(
            tab_id_webext,
            tab_id_tmp_dirs.get(crawl_type).unwrap(),
            crawl_type,
        );

        let tools = &driver_tools.get(&crawl_type).unwrap().tools;
        install_temp_local_webext(tools, Path::new(&tab_id_tmp_dir))
            .await
            .context(format!("Could not install WebExtension {tab_id_webext}"))?;

        for webext_path in browser_webexts.get(crawl_type).unwrap() {
            install_temp_local_webext(tools, Path::new(webext_path))
                .await
                .context(format!("Could not install WebExtension {webext_path}"))?;
        }
    }

    let output_base_dir: &str = &Path::new("data")
        .join(format!("crawl_{}", crawl_framework::millis_since_epoch()))
        .to_string_lossy()
        .to_string();

    // Create data output directories
    let output_dirs = WEB_DRIVER_PORTS
        .iter()
        .map(|(crawl_type, _)| {
            let output_dir = format!("{output_base_dir}_{}", crawl_type);
            #[allow(clippy::unwrap_used)]
            fs::create_dir_all(&output_dir)
                .with_context(|| format!("Could not create the output directory {output_dir}"))
                .unwrap();
            (crawl_type, output_dir)
        })
        .collect::<HashMap<_, _>>();

    let mut sessions = driver_tools
        .iter()
        .map(|(&crawl_type, driver_tool)| {
            (
                crawl_type,
                Arc::new(tokio::sync::Mutex::new(driver_tool.driver.handle.clone())),
            )
        })
        .collect::<HashMap<_, _>>();

    let (net_idle_sender, net_idle_receiver) = crossbeam_channel::unbounded();

    let tab_idle_waiter = Arc::new(TabIdleWaiter::new(net_idle_receiver.clone()));

    let data_manager = Arc::new(
        DataManager::new("crawl_data.db")
            .await
            .context("Could not create the DataManager")?,
    );
    let manager = Arc::clone(&data_manager);

    // Set up the data-receiver HTTP server
    let addr = SocketAddr::from(([127, 0, 0, 1], 3000));
    let new_service = make_service_fn(move |_conn| {
        let sender = net_idle_sender.clone();
        let m = Arc::clone(&manager);
        async {
            Ok::<_, Infallible>(service_fn(move |req: Request<Body>| {
                data_rx_server::route(sender.clone(), Arc::clone(&m), req)
            }))
        }
    });
    let server = Server::bind(&addr).serve(new_service);
    tokio::spawn(server);

    let mut tasks = tokio::task::JoinSet::new();

    let tab_counts = WEB_DRIVER_PORTS
        .iter()
        .map(|(crawl_type, _)| (crawl_type.clone(), Arc::new(tokio::sync::RwLock::new(0))))
        .collect::<HashMap<_, _>>();

    loop {
        // Enforce a maximum open tab count limit
        loop {
            let mut all_browsers_ready = true;
            for tab_count in tab_counts.values() {
                let tab_count_guard = tab_count.read().await;
                if *tab_count_guard >= MAX_OPEN_TABS {
                    all_browsers_ready = false;
                }
            }
            if all_browsers_ready {
                break;
            }

            // Avoid busy waiting
            tokio::time::sleep(Duration::from_millis(10)).await;
        }

        let (next_crawl_task, crawl_tasks_left_count) = {
            let mut crawl_tasks_guard = crawl_tasks.lock().await;
            (crawl_tasks_guard.pop(), crawl_tasks_guard.iter().count())
        };
        let crawl_task = if let Some(crawl_task) = next_crawl_task {
            crawl_task
        } else {
            // Wait for all running tasks to be finished
            while tasks.join_one().await.is_some() {}

            // Check whether new crawl tasks have been added (for children pages)
            let mut crawl_tasks_guard = crawl_tasks.lock().await;
            if let Some(crawl_task) = crawl_tasks_guard.pop() {
                crawl_task
            } else {
                break;
            }
        };

        let url = crawl_task.url.clone();
        info!(
            "Starting {url} ({} crawl tasks remaining)",
            crawl_tasks_left_count
        );

        let crawl_tasks_handle = Arc::clone(&crawl_tasks);

        let data_manager_handle = Arc::clone(&data_manager);

        let mut session_handles = sessions
            .iter()
            .map(|(&crawl_type, session)| (crawl_type, Arc::clone(session)))
            .collect::<HashMap<_, _>>();
        let mut tools_handles = driver_tools
            .iter()
            .map(|(&crawl_type, driver_tool)| (crawl_type, driver_tool.tools.clone()))
            .collect::<HashMap<_, _>>();

        let tab_count_handles = tab_counts
            .iter()
            .map(|(crawl_type, tab_count)| (crawl_type.clone(), Arc::clone(tab_count)))
            .collect::<HashMap<_, _>>();

        let tab_idle_waiter_handles = WEB_DRIVER_PORTS
            .iter()
            .map(|(crawl_type, _)| (crawl_type, Arc::clone(&tab_idle_waiter)))
            .collect::<HashMap<_, _>>();

        let mut new_tab_handles = futures::future::join_all(session_handles.iter().map(
            |(&crawl_type, session_handle)| {
                let tab_counts = tab_counts.clone();
                async move {
                    let new_tab_handle = match crawl_framework::open_new_tab(session_handle).await {
                        Ok(new_tab_handle) => Some(new_tab_handle),
                        Err(err) => {
                            error!("Could not open a new tab in the {crawl_type} browser: {err}");

                            None
                        }
                    };

                    if new_tab_handle != None {
                        let mut tab_count_guard = tab_counts.get(crawl_type).unwrap().write().await;
                        *tab_count_guard += 1;
                    }

                    (crawl_type, new_tab_handle)
                }
            },
        ))
        .await
        .into_iter()
        .collect::<HashMap<_, _>>();

        // Restart the browsers that crashed, if any
        for (crawl_type, new_tab_handle) in new_tab_handles.clone() {
            if new_tab_handle.is_some() {
                continue;
            }

            #[allow(clippy::unwrap_used)]
            driver_tools
                .remove(crawl_type)
                .unwrap()
                .driver
                .quit()
                .await
                .with_context(|| format!("Could not quit the driver for the {crawl_type} crawl"))
                .unwrap();

            #[allow(clippy::unwrap_used)]
            geckodrivers
                .get_mut(crawl_type)
                .unwrap()
                .kill()
                .with_context(|| {
                    format!("Could not kill the geckodriver for the {crawl_type} crawl")
                })
                .unwrap();

            let web_driver_port = WEB_DRIVER_PORTS.get(crawl_type).unwrap();

            let driver = geckodrivers.get_mut(crawl_type).unwrap();
            *driver = spawn_geckodriver(*web_driver_port);

            // Wait for geckodriver to start
            tokio::time::sleep(Duration::from_secs(3)).await;

            #[allow(clippy::unwrap_used)]
            let driver_tool =
                setup_firefox_driver(*web_driver_port, &PageLoadStrategy::Eager, HEADLESS)
                    .await
                    .with_context(|| {
                        format!("Could not set up a WebDriver driver for the {crawl_type} browser")
                    })
                    .unwrap();

            let session = sessions.get_mut(crawl_type).unwrap();
            *session = Arc::new(tokio::sync::Mutex::new(driver_tool.driver.handle.clone()));

            let session_handle = session_handles.get_mut(crawl_type).unwrap();
            *session_handle = Arc::clone(session);

            let tools_handle = tools_handles.get_mut(crawl_type).unwrap();
            *tools_handle = driver_tool.tools.clone();

            driver_tools.insert(crawl_type, driver_tool);

            let new_tab_handle = new_tab_handles.get_mut(crawl_type).unwrap();
            #[allow(clippy::unwrap_used)]
            {
                *new_tab_handle = Some(
                    crawl_framework::open_new_tab(session_handle)
                        .await
                        .with_context(|| {
                            "Could not open a new tab in the {crawl_type} browser: {err}"
                        })
                        .unwrap(),
                );
            }

            let mut tab_count_guard = tab_counts.get(crawl_type).unwrap().write().await;
            // Reset the counter
            *tab_count_guard = 1;
        }

        for (crawl_type, _) in WEB_DRIVER_PORTS.iter() {
            let output_dir = output_dirs.get(&crawl_type).unwrap().clone();

            let crawl_tasks_handle = Arc::clone(&crawl_tasks_handle);
            let crawl_task = crawl_task.clone();
            let session_handles = session_handles.clone();
            let new_tab_handles = new_tab_handles.clone();
            let tools_handles = tools_handles.clone();
            let tab_idle_waiter_handles = tab_idle_waiter_handles.clone();
            let data_manager_handle = Arc::clone(&data_manager_handle);
            let tab_count_handles = tab_count_handles.clone();
            let url = url.clone();

            tasks.spawn(async move {
                let session_handle = session_handles.get(&crawl_type).unwrap();
                // This unwrap would panic if we were not handle to open a new tab, but this is
                // unrecoverable and we have already logged the error
                #[allow(clippy::unwrap_used)]
                let new_tab_handle = new_tab_handles.get(&crawl_type).unwrap().clone().unwrap();

                let check_page_res = inspect_page::inspect_page(
                    session_handle,
                    new_tab_handle.clone(),
                    tools_handles.get(&crawl_type).unwrap(),
                    tab_idle_waiter_handles.get(&crawl_type).unwrap(),
                    &data_manager_handle,
                    crawl_type,
                    &crawl_tasks_handle,
                    &crawl_task,
                    Path::new(&output_dir),
                )
                .await;

                if let Err(err) = check_page_res {
                    debug!("Error while inspecting page {url} in {crawl_type} browser: {err}");
                }

                if let Err(err) = crawl_framework::close_tab(session_handle, new_tab_handle).await {
                    debug!("Error while closing a tab for {url} in {crawl_type} browser: {err}");
                }

                let mut tab_count_guard = tab_count_handles.get(crawl_type).unwrap().write().await;
                *tab_count_guard -= 1;
            });
        }
    }

    for (_, driver_tool) in driver_tools {
        driver_tool.driver.quit().await?;
    }

    for (crawl_type, mut driver) in geckodrivers {
        #[allow(clippy::unwrap_used)]
        driver
            .kill()
            .with_context(|| format!("Could not kill the geckodriver for the {crawl_type} crawl"))
            .unwrap();
    }

    Ok(())
}
