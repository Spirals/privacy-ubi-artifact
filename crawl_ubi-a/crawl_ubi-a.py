# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.13.8
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %% [markdown] jp-MarkdownHeadingCollapsed=true tags=[] jp-MarkdownHeadingCollapsed=true tags=[] jp-MarkdownHeadingCollapsed=true jp-MarkdownHeadingCollapsed=true tags=[] jp-MarkdownHeadingCollapsed=true tags=[]
# ## Imports and configuration

# %%
import json
import os
from sqlite3 import connect
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
import pandas as pd
import seaborn as sns

conn = connect('../crawl_ubi-a_data/crawl_ubi-a_2/all_crawl_data.db')

# %% [markdown]
# Matplotlib configuration:

# %%
matplotlib.rcParams['pdf.fonttype'] = 42

# %% [markdown]
# Pandas configuration:

# %%
pd.set_option('display.max_colwidth', None)
pd.set_option('display.max_rows', None)
pd.set_option('styler.latex.hrules', True)
pd.set_option('styler.format.precision', 2)

# %% [markdown]
# Seaborn configuration:

# %%
custom_params = {'axes.spines.right': False, 'axes.spines.top': False}
sns.set_theme(style='ticks', palette='colorblind', rc=custom_params)

# %% [markdown] tags=[] jp-MarkdownHeadingCollapsed=true tags=[] jp-MarkdownHeadingCollapsed=true tags=[] jp-MarkdownHeadingCollapsed=true jp-MarkdownHeadingCollapsed=true tags=[] jp-MarkdownHeadingCollapsed=true jp-MarkdownHeadingCollapsed=true tags=[] jp-MarkdownHeadingCollapsed=true
# ## General crawl statistics

# %% [markdown]
# Count of `js` pages:

# %%
pd.read_sql_query(
    '''
    SELECT COUNT(DISTINCT crawl_url) FROM crawl_data
    WHERE crawl_type == 'js'
    ''',
    conn,
    ).iloc[0, 0]

# %% [markdown]
# Count of `nojs` pages:

# %%
pd.read_sql_query(
    '''
    SELECT COUNT(DISTINCT crawl_url) FROM crawl_data
    WHERE crawl_type == 'nojs'
    ''',
    conn,
    ).iloc[0, 0]

# %% [markdown]
# Count of `nojs_ubi` pages:

# %% tags=[]
nojs_ubi_page_count = pd.read_sql_query(
    '''
    SELECT COUNT(DISTINCT crawl_url) FROM crawl_data
    WHERE crawl_type == 'nojs_ubi'
    ''',
    conn,
    ).iloc[0, 0]
nojs_ubi_page_count

# %% [markdown]
# Share of internal pages (%):

# %%
pd.read_sql_query(
    '''
    SELECT
        100 * CAST((
            SELECT COUNT(DISTINCT crawl_url) FROM crawl_data
            WHERE landing_page == '0') AS REAL
        )
        / (SELECT COUNT(DISTINCT crawl_url) FROM crawl_data)
    ''',
    conn,
    ).iloc[0, 0]

# %% [markdown]
# Pages having data saved for the three crawl types:

# %% tags=[]
pd.read_sql_query(
    '''
    SELECT COUNT(*)
    FROM (
        SELECT DISTINCT crawl_url FROM crawl_data
        WHERE crawl_type == 'js'
        )

    INNER JOIN (
        SELECT DISTINCT crawl_url FROM crawl_data
        WHERE crawl_type == 'nojs'
        )
    USING (crawl_url)

    INNER JOIN (
        SELECT DISTINCT crawl_url FROM crawl_data
        WHERE crawl_type == 'nojs_ubi'
        )
    USING (crawl_url)
    ''',
    conn,
    ).iloc[0, 0]

# %% [markdown] tags=[] jp-MarkdownHeadingCollapsed=true tags=[] jp-MarkdownHeadingCollapsed=true tags=[] jp-MarkdownHeadingCollapsed=true
# ## Repair statistics

# %% tags=[]
repaired_element_counts_url = pd.read_sql_query(
    '''
    SELECT
        crawl_url,
        ranking,
        landing_page,
        CAST(data ->> 'lazyLoadedImageCount'     AS INTEGER) AS 'Lazy-loaded images',
        CAST(data ->> 'fadeinElementsShownCount' AS INTEGER) AS 'Fade-in elements',
        CAST(data ->> 'lazyStylesheetCount'      AS INTEGER) AS 'Lazy-loaded stylesheets',
        CAST(data ->> 'noscriptTagCount'         AS INTEGER) AS 'Noscript tags',
        CAST(data ->> 'hiddenPreloaderCount'     AS INTEGER) AS 'Preloaders'
    FROM (
        SELECT crawl_url, ranking, landing_page, data FROM crawl_data
        WHERE crawl_type == 'nojs_ubi'
        GROUP BY crawl_url
        )
    ''',
    conn,
    )

repaired_element_labels = [
    'Lazy-loaded images',
    'Fade-in elements',
    'Lazy-loaded stylesheets',
    'Noscript tags',
    'Preloaders',
    ]

# %%
repaired_element_counts_url[repaired_element_counts_url['Preloaders'] > 0].reset_index(drop=True).truncate(after=10)

# %%
repaired_element_counts_url[repaired_element_counts_url['Fade-in elements'] > 0] \
    .reset_index(drop=True) \
    .sample(frac=1) \
    .reset_index(drop=True) \
    .truncate(after=10)

# %%
repaired_element_counts = repaired_element_counts_url.drop(columns=['crawl_url'])

# %%
repair_corr = (repaired_element_counts \
    .drop(columns=['ranking', 'landing_page']) > 0) \
    .corr()
repair_corr

# %%
ax = sns.heatmap(repair_corr, cmap="YlGnBu", square=True)
ax.set_xticklabels(ax.get_xticklabels(), rotation=30, ha='right', rotation_mode='anchor');
plt.gcf().savefig('../paper-ubi-a/repair_correlation_matrix.pdf', bbox_inches='tight')

# %% [markdown]
# Total counts of repaired elements:

# %% tags=[]
repaired_element_counts_pos = \
    repaired_element_counts[repaired_element_counts > 0]
repaired_element_counts_pos['landing_page'] = repaired_element_counts_pos['landing_page'].replace(np.nan, 0)

# %% [markdown]
# Counts of `nojs_ubi` pages where at least an element has been repaired:

# %%
repaired_page_counts = repaired_element_counts_pos \
    .groupby('landing_page') \
    .count() \
    .transpose()
repaired_page_counts = repaired_page_counts \
    .drop(index=['ranking']) \
    .rename_axis(columns='') \
    .rename(columns={0: 'Count of internal pages', 1: 'Count of homepages'})
repaired_page_counts['Total count of pages'] = repaired_element_counts_pos.count()
repaired_page_counts

# %% [markdown]
# Shares of `nojs_ubi` pages having an element that has been repaired:

# %%
has_repaired_elements = repaired_element_counts \
    .drop(columns=['ranking'])
has_repaired_elements['\\emph{Any}'] = has_repaired_elements.drop(columns=['landing_page']).sum(axis='columns') > 0
has_repaired_elements.truncate(after=15)

# %%
has_repaired_elements[repaired_element_labels] = (has_repaired_elements[repaired_element_labels] > 0) \
    .replace({True: 1, False: 0})
repaired_element_page_shares = has_repaired_elements \
    .groupby('landing_page') \
    .mean() \
    .transpose() \
    .rename_axis(columns='') \
    .rename(columns={0: 'Share of internal pages (\%)', 1: 'Share of homepages (\%)'})
repaired_element_page_shares['Total share of pages (\%)'] = has_repaired_elements.mean()
perc_page_repaired_elements = 100 * repaired_element_page_shares
perc_page_repaired_elements

# %%
perc_page_repaired_elements.style.to_latex(
    '../paper-ubi-a/table_perc_repairs.tex',
    position='t',
    environment='table*',
    label='tab:perc-repairs',
    caption='Shares of pages among the \\pagecrawlcount{} where repairs have been applied to at least one element',
)

# %%
repaired_element_counts_pos_wide = repaired_element_counts_pos \
    .melt(
        id_vars=['ranking', 'landing_page'],
        value_vars=repaired_element_labels,
        var_name='repair',
        value_name='repair_count',
    )
repaired_element_counts_pos_wide;

# %%
grid = sns.catplot(
    data=repaired_element_counts_pos_wide.drop(columns=['ranking'])
        .rename(columns={'landing_page': 'Page type'})
        .replace({'Page type': {0: 'Internal', 1: 'Homepage'}}),
    y='repair',
    x='repair_count',
    kind='box',
    hue='Page type',
    height=4.2,
    aspect=1.4,
    orient='h',
    )
grid.set(
    ylabel='',
    xlabel='',
    xscale='log',
    )
grid.ax.set_xticks(
    [x for x in grid.ax.get_xticks(minor=True)
        if x > 1 and x < repaired_element_counts_pos.max().max() / 10],
    minor=True,
    );
sns.move_legend(grid, "lower right", bbox_to_anchor=(0.85, 0.15))
plt.gcf().savefig('../paper-ubi-a/counts_repaired_elements_per_page.pdf', bbox_inches='tight')

# %% [markdown] tags=[] jp-MarkdownHeadingCollapsed=true jp-MarkdownHeadingCollapsed=true tags=[] jp-MarkdownHeadingCollapsed=true jp-MarkdownHeadingCollapsed=true tags=[] jp-MarkdownHeadingCollapsed=true jp-MarkdownHeadingCollapsed=true tags=[] jp-MarkdownHeadingCollapsed=true
# ## Background color statistics (unused)

# %% tags=[]
background_colors = pd.read_sql_query(
    '''
    SELECT data ->> 'backgroundColor'
    FROM (
        SELECT data FROM crawl_data
        WHERE crawl_type == 'nojs_ubi'
        GROUP BY crawl_url
        )
    ''',
    conn,
    ).rename(columns={"data ->> 'backgroundColor'": 'background_color'})

background_colors \
     .value_counts() \
    .reset_index() \
    .rename(columns={'background_color': 'Background color', 0: 'Count'}) \
    .truncate(after=10)

# %% [markdown] tags=[] jp-MarkdownHeadingCollapsed=true tags=[] jp-MarkdownHeadingCollapsed=true tags=[] jp-MarkdownHeadingCollapsed=true jp-MarkdownHeadingCollapsed=true tags=[] jp-MarkdownHeadingCollapsed=true jp-MarkdownHeadingCollapsed=true tags=[] jp-MarkdownHeadingCollapsed=true
# ## Page timing statistics

# %% tags=[]
page_timings = pd.read_sql_query(
    '''
    SELECT
        js.crawl_url,

        CAST(js.page_timings ->> 'interactive_time' AS INTEGER) AS 'JS DOMContentLoaded',
        CAST(js.page_timings ->> 'load_time'        AS INTEGER) AS 'JS load',
        CAST(js.page_timings ->> 'post_load_time'   AS INTEGER) AS 'JS post_load_time',
        CAST(js.page_timings ->> 'total_load_time'  AS INTEGER) AS 'JS Network idle for 5 s',
        
        CAST(nojs.page_timings ->> 'interactive_time' AS INTEGER) AS 'NoJS DOMContentLoaded',
        CAST(nojs.page_timings ->> 'load_time'        AS INTEGER) AS 'NoJS load',
        CAST(nojs.page_timings ->> 'post_load_time'   AS INTEGER) AS 'NoJS post_load_time',
        CAST(nojs.page_timings ->> 'total_load_time'  AS INTEGER) AS 'NoJS Network idle for 5 s',
        
        CAST(nojs_ubi.page_timings ->> 'interactive_time' AS INTEGER) AS 'NoJS-UBI DOMContentLoaded',
        CAST(nojs_ubi.page_timings ->> 'load_time'        AS INTEGER) AS 'NoJS-UBI load',
        CAST(nojs_ubi.page_timings ->> 'post_load_time'   AS INTEGER) AS 'NoJS-UBI post_load_time',
        CAST(nojs_ubi.page_timings ->> 'total_load_time'  AS INTEGER) AS 'NoJS-UBI Network idle for 5 s'

    FROM (
        SELECT crawl_url, page_timings FROM crawl_data
        WHERE crawl_type == 'js'
        GROUP BY crawl_url
        ) AS js
    
    INNER JOIN (
        SELECT crawl_url, page_timings FROM crawl_data
        WHERE crawl_type == 'nojs'
        GROUP BY crawl_url
        ) AS nojs
    USING (crawl_url)

    INNER JOIN (
        SELECT crawl_url, page_timings FROM crawl_data
        WHERE crawl_type == 'nojs_ubi'
        GROUP BY crawl_url
        ) AS nojs_ubi
    USING (crawl_url)
    ''',
    conn
    )

# %% tags=[]
page_timings_box = page_timings.drop(columns=['crawl_url'])
page_timings_box = page_timings_box / 1000 # millis to seconds
page_timings_box = page_timings_box[page_timings_box['JS Network idle for 5 s'] < 1000] # Remove buggy outliers
page_timings_box = page_timings_box.rename_axis(index='Time', columns='Page event')
page_timings_box.truncate(after=5)

# %% tags=[]
grid = sns.displot(
    data=page_timings_box,
    kind='kde',
    log_scale=True,
    height=8,
    )
sns.move_legend(grid, "lower center", bbox_to_anchor=(.5, 1), ncol=3, title=None, frameon=False)
grid.set(
    xlabel='Time (s)',
    );

# %% tags=[]
page_timings_box['NoJS/JS load speedup'] = 100 * (page_timings_box['JS load'] - page_timings_box['NoJS load']) / page_timings_box['JS load']
page_timings_box['NoJS-UBI/NoJS load speedup'] = 100 * (page_timings_box['NoJS load'] - page_timings_box['NoJS-UBI load']) / page_timings_box['NoJS load']
page_timings_box.truncate(after=5)

# %%
grid = sns.catplot(
    data=page_timings_box['NoJS/JS load speedup'].to_frame(),
    kind='box',
    orient='h',
    height=1.4,
    aspect=4,
    )
grid.ax.xaxis.set_major_locator(MultipleLocator(20))
grid.ax.xaxis.set_minor_locator(MultipleLocator(10))
grid.set(
    ylabel='NoJS/JS',
    yticklabels=[],
    xlim=[-50, 105], # Some outliers omitted
    xlabel='Page load speedup (%)',
);
plt.gcf().savefig('../paper-ubi-a/speedup_nojs_js.pdf', bbox_inches='tight')

# %%
page_timings_box.median()

# %%
grid = sns.catplot(
    data=page_timings_box['NoJS-UBI/NoJS load speedup'].to_frame(),
    kind='box',
    orient='h',
    height=1.4,
    aspect=4.3,
    )
grid.ax.xaxis.set_major_locator(MultipleLocator(10))
grid.ax.xaxis.set_minor_locator(MultipleLocator(5))
grid.set(
    ylabel='NoJS-UBI/NoJS',
    yticklabels=[],
    xlim=[-35, 35], # Some outliers omitted
    xlabel='Page load speedup (%)',
);
plt.gcf().savefig('../paper-ubi-a/speedup_nojs-ubi_nojs.pdf', bbox_inches='tight')

# %%

# %% [markdown] tags=[] jp-MarkdownHeadingCollapsed=true jp-MarkdownHeadingCollapsed=true tags=[] jp-MarkdownHeadingCollapsed=true jp-MarkdownHeadingCollapsed=true tags=[] jp-MarkdownHeadingCollapsed=true
# ## Tracking request statistics

# %% tags=[]
crawl_requests = pd.read_sql_query(
    '''
    SELECT
        js.crawl_url,
        js.data       ->> 'requestList' AS 'JS requests',
        nojs.data     ->> 'requestList' AS 'NoJS requests',
        nojs_ubi.data ->> 'requestList' AS 'NoJS-UBI requests'

    FROM (
        SELECT crawl_url, data FROM crawl_data
        WHERE crawl_type == 'js'
        GROUP BY crawl_url
        ) AS js

    INNER JOIN (
        SELECT crawl_url, data FROM crawl_data
        WHERE crawl_type == 'nojs'
        GROUP BY crawl_url
        ) AS nojs
    USING (crawl_url)

    INNER JOIN (
        SELECT crawl_url, data FROM crawl_data
        WHERE crawl_type == 'nojs_ubi'
        GROUP BY crawl_url
        ) AS nojs_ubi
    USING (crawl_url)
    ''',
    conn,
    )

# %% tags=[]
crawl_requests.truncate(after=5)

# %% tags=[]
pd.json_normalize(json.loads(crawl_requests['JS requests'].iloc[2]))


# %%
def is_tracking_request(req) -> bool:
    # https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/webRequest/onBeforeRequest#urlclassification
    tracking_labels = ['any_basic_tracking']
    return (
        req.thirdParty and any(l in req['urlClassification.thirdParty'] for l in tracking_labels)
        or (not req.thirdParty and any(l in req['urlClassification.firstParty'] for l in tracking_labels))
    )

def count_tracking_requests(json_requests: str | None) -> int:
    if json_requests == None:
        return None
    return pd.json_normalize(json.loads(json_requests)).apply(is_tracking_request, axis='columns').sum()

def get_tracking_requests(json_requests: str | None):
    requests = get_requests(json_requests)
    return requests[requests.apply(is_tracking_request, axis='columns')]

def get_requests(json_requests: str | None):
    if json_requests == None:
        return pd.DataFrame()
    return pd.json_normalize(json.loads(json_requests))


# %% [markdown]
# Tracking requests with JS enabled:

# %%
js_tracking_requests = pd.concat(
    crawl_requests['JS requests'] \
        .apply(get_tracking_requests) \
        .values
    ) \
    .reset_index(drop=True)

# %% tags=[]
js_tracking_requests.shape[0]

# %% [markdown]
# Tracking requests with JS disabled:

# %% tags=[]
nojs_ubi_tracking_requests = pd.concat(
    crawl_requests['NoJS-UBI requests'] \
        .apply(get_tracking_requests) \
        .values
    ) \
    .reset_index(drop=True)

# %%
nojs_ubi_tracking_requests.shape[0]

# %% tags=[]
nojs_ubi_tracking_requests.truncate(after=100)

# %%
thirdparty_nojs_ubi_tracking_requests = nojs_ubi_tracking_requests[nojs_ubi_tracking_requests['thirdParty'] == True] \
    .reset_index(drop=True) \
    .groupby('type') \
    .count()['url'] \
    .to_frame() \
    .rename(columns={ 'url': 'NoJS-UBI' }) \
    .sort_values(by='NoJS-UBI', ascending=False)
thirdparty_nojs_ubi_tracking_requests

# %% [markdown]
# Third-party tracking requests grouped by request type:

 # %% tags=[]
 thirdparty_js_tracking_requests = js_tracking_requests[js_tracking_requests['thirdParty'] == True] \
    .reset_index(drop=True) \
    .groupby('type') \
    .count()['url'] \
    .to_frame() \
    .rename(columns={ 'url': 'JS' }) \
    .join(thirdparty_nojs_ubi_tracking_requests) \
    .replace(np.nan, 0) \
    .convert_dtypes() \
    .sort_values(by='JS', ascending=False) \
    .rename_axis('Request type') \
    .rename(
        columns={ 'url': 'Count' },
        index={
            'image': 'Image',
            'sub_frame': 'Iframe',
            'stylesheet': 'Stylesheet',
            'other': 'Other',
            'script': 'Script',
            'font': 'Font',
            'beacon': 'Beacon',
            'media': 'Media',
            'csp_report': 'CSP report',
            'xmlhttprequest': 'XHR',
            'websocket': 'WebSocket',
            'object': 'Object',
            'imageset': 'Image set',
        },
    )
thirdparty_js_tracking_requests

# %%
thirdparty_js_tracking_requests.style.to_latex(
    '../paper-ubi-a/table_thirdparty_tracking_requests.tex',
    label='tab:thirdparty-tracking-requests',
    caption='Counts of third-party tracking requests with \\javascript{} enabled and disabled, with our repairs. Some Script and XHR requests are still without \\javascript{} present because JSON objects loaded with \\htmltag{script} tags and \\htmltag{link} preloads are classified as such.',
)

# %%
crawl_tracking_request_counts = crawl_requests.copy()
crawl_tracking_request_counts = crawl_tracking_request_counts.rename(columns={
    'JS requests': 'JS tracking request count',
    'NoJS requests': 'NoJS tracking request count',
    'NoJS-UBI requests': 'NoJS-UBI tracking request count',
    })
crawl_tracking_request_counts['JS tracking request count'] \
    = crawl_tracking_request_counts['JS tracking request count'].transform(count_tracking_requests)
crawl_tracking_request_counts['NoJS tracking request count'] \
    = crawl_tracking_request_counts['NoJS tracking request count'].transform(count_tracking_requests)
crawl_tracking_request_counts['NoJS-UBI tracking request count'] \
    = crawl_tracking_request_counts['NoJS-UBI tracking request count'].transform(count_tracking_requests)
crawl_tracking_request_counts.truncate(after=10)

# %%
crawl_tracking_request_counts['NoJS-UBI tracking reduction (%)'] \
    = 100 * (crawl_tracking_request_counts['JS tracking request count'] - crawl_tracking_request_counts['NoJS-UBI tracking request count']) \
        / crawl_tracking_request_counts['JS tracking request count']
crawl_tracking_request_counts.truncate(after=10)

# %% tags=[]
crawl_tracking_request_counts[crawl_tracking_request_counts['NoJS tracking request count'] > 0] \
    .reset_index(drop=False) \
    .truncate(after=10)

# %% [markdown]
# Pages where there wasn't any tracking request with JS and the UBI webext seems to trigger at least one:

# %%
crawl_tracking_request_counts[crawl_tracking_request_counts['NoJS-UBI tracking reduction (%)'] == -np.inf]

# %% [markdown]
# Median reduction of tracking requests when using the UBI webext:

# %%
grid = sns.displot(
    data=crawl_tracking_request_counts['NoJS-UBI tracking reduction (%)'] \
        .replace([np.inf, -np.inf], np.nan).to_frame(),
    kind='ecdf',
    )
sns.move_legend(grid, "upper left", bbox_to_anchor=(0.1, 0.9))
grid.set(
    xlim=[0, 100],
    xlabel='NoJS-UBI tracking reduction (%)',
    );

# %%
crawl_tracking_request_counts['NoJS-UBI tracking reduction (%)'] \
    .replace([np.inf, -np.inf], np.nan) \
    .median()

# %% [markdown]
# Mean reduction of tracking requests when using the UBI webext:

# %%
crawl_tracking_request_counts['NoJS-UBI tracking reduction (%)'] \
    .replace([np.inf, -np.inf], np.nan) \
    .mean()

# %% [markdown]
# Standard deviation of the reduction of tracking requests when using the UBI webext:

# %%
crawl_tracking_request_counts['NoJS-UBI tracking reduction (%)'] \
    .replace([np.inf, -np.inf], np.nan) \
    .std()

# %% [markdown] tags=[] jp-MarkdownHeadingCollapsed=true tags=[] jp-MarkdownHeadingCollapsed=true tags=[] jp-MarkdownHeadingCollapsed=true jp-MarkdownHeadingCollapsed=true tags=[] jp-MarkdownHeadingCollapsed=true tags=[] jp-MarkdownHeadingCollapsed=true
# ## Cookie annoyance reduction

# %%
cookie_annoyance_counts = pd.read_sql_query(
    '''
    SELECT
        js.crawl_url,
        js.ranking,
        js.landing_page,
        CAST(js.data       ->> 'cookieannoyancecount' AS INTEGER) AS 'JS cookie annoyance count',
        CAST(nojs.data     ->> 'cookieannoyancecount' AS INTEGER) AS 'NoJS cookie annoyance count',
        CAST(nojs_ubi.data ->> 'cookieannoyancecount' AS INTEGER) AS 'NoJS-UBI cookie annoyance count'

    FROM (
        SELECT crawl_url, ranking, landing_page, data FROM crawl_data
        WHERE crawl_type == 'js'
        GROUP BY crawl_url
        ) AS js

    INNER JOIN (
        SELECT crawl_url, landing_page, data FROM crawl_data
        WHERE crawl_type == 'nojs'
        GROUP BY crawl_url
        ) AS nojs
    USING (crawl_url)

    INNER JOIN (
        SELECT crawl_url, landing_page, data FROM crawl_data
        WHERE crawl_type == 'nojs_ubi'
        GROUP BY crawl_url
        ) AS nojs_ubi
    USING (crawl_url)
    ''',
    conn,
    )

# %%
cookie_annoyance_counts.truncate(after=10)

# %%
cookie_annoyance_counts['Diff NoJS-UBI - NoJS'] \
    = cookie_annoyance_counts['NoJS-UBI cookie annoyance count'] \
    - cookie_annoyance_counts['NoJS cookie annoyance count']

# %% [markdown]
# Pages where the UBI webext seemed to have made some cookie annoyance appear:

# %%
cookie_annoyance_counts[cookie_annoyance_counts['Diff NoJS-UBI - NoJS'] > 0]

# %% [markdown]
# Share of JS pages having a cookie annoyance (%):

# %%
100 * (cookie_annoyance_counts['JS cookie annoyance count'] > 0).value_counts(normalize=True).get(True)

# %% [markdown]
# Shares of pages having some cookie annoyance on the JS page where it is all removed by the UBI webext (%):

# %%
js_pages_with_cookie_annoyance = cookie_annoyance_counts[cookie_annoyance_counts['JS cookie annoyance count'] > 0]
100 * (js_pages_with_cookie_annoyance['NoJS-UBI cookie annoyance count'] == 0).value_counts(normalize=True).get(True)

# %% [markdown] tags=[] jp-MarkdownHeadingCollapsed=true tags=[] jp-MarkdownHeadingCollapsed=true jp-MarkdownHeadingCollapsed=true tags=[] jp-MarkdownHeadingCollapsed=true tags=[] jp-MarkdownHeadingCollapsed=true
# ## Screenshot analysis preparation

# %%
screenshot_paths = pd.read_sql_query(
     '''
     SELECT
        js.screenshot_paths       ->> 'main' AS 'js_screenshot_path',
        nojs.screenshot_paths ->> 'main' AS 'nojs_screenshot_path',
        nojs_ubi.screenshot_paths ->> 'main' AS 'nojs-ubi_screenshot_path'

    FROM (
        SELECT crawl_url, screenshot_paths FROM crawl_data
        WHERE crawl_type == 'js'
        GROUP BY crawl_url
        ) AS js

    INNER JOIN (
        SELECT crawl_url, screenshot_paths FROM crawl_data
        WHERE crawl_type == 'nojs'
        GROUP BY crawl_url
        ) AS nojs
    USING (crawl_url)
    
    INNER JOIN (
        SELECT crawl_url, screenshot_paths FROM crawl_data
        WHERE crawl_type == 'nojs_ubi'
        GROUP BY crawl_url
        ) AS nojs_ubi
    USING (crawl_url)
    ''',
    conn,
    )

# %% [markdown]
# Save the list of screenshots that should be diffed automatically using `browser-screenshot-diff`

# %%
screenshot_to_diff_paths = screenshot_paths.drop(columns=['nojs_screenshot_path']).dropna()


# %%
def build_output_diff_path(path):
    return 'output_diffs/diff/' + os.path.basename(path).replace('js', 'diff')


# %%
screenshot_to_diff_paths['diff_screenshot_path'] = screenshot_to_diff_paths['js_screenshot_path'] \
    .apply(build_output_diff_path)
screenshot_to_diff_paths.to_csv('screenshots_to_diff.tsv', sep='\t', header=False, index=False)

# %% [markdown] tags=[] jp-MarkdownHeadingCollapsed=true tags=[] jp-MarkdownHeadingCollapsed=true
# ## Screenshot analysis

# %%
conn_diff = connect('../crawl_ubi-a_data/crawl_ubi-a_2/diff_stats.db')

# %%
pd.read_sql_query(
    '''
    SELECT COUNT(*) FROM diff_stats
    ''',
    conn_diff,
    ).iloc[0, 0]

# %%
diff_pixels_perc = pd.read_sql_query(
    '''
    SELECT
        height0,
        height1,
        diff_pixels_perc AS 'Pixel difference (%)'
    FROM diff_stats
    ''',
    conn_diff,
    )

# %% [markdown]
# Count of pages having a pixel difference of 0 %:

# %%
(diff_pixels_perc['Pixel difference (%)'] == 0).sum()

# %% [markdown]
# Count of pages having a pixel difference of at most 2 %:

# %%
(diff_pixels_perc['Pixel difference (%)'] < 2).sum()

# %%
grid = sns.displot(
    data=diff_pixels_perc,
    x='Pixel difference (%)',
    kind='hist',
    bins=list(range(0, 150, 2)),
    height=4.2,
    aspect=1.6,
    )
plt.gcf().savefig('../paper-ubi-a/hist_pixel_difference.pdf', bbox_inches='tight')

# %%
grid = sns.displot(
    data=diff_pixels_perc,
    x='Pixel difference (%)',
    kind='hist',
    bins=list(range(150, 3000, 10)),
)

# %%
grid = sns.displot(
    data=diff_pixels_perc,
    x='Pixel difference (%)',
    kind='ecdf',
    height=3.5,
    aspect=1,
)
grid.set(
    xlim=(0, 150),
    );
plt.gcf().savefig('../paper-ubi-a/cdf_pixel_difference.pdf', bbox_inches='tight')

# %%
grid = sns.displot(
    data=diff_pixels_perc,
    x='height0',
    kind='hist',
)
grid.set(
    xlabel='Height of JS page screenshot (px)',
    );

# %%
perc_bins = [0, 1, 3, 5, 10, 20, 35, 50, 65, 80, 100, 150, 200, np.inf]

# %%
sns.lineplot(data=perc_bins);

# %%
diff_pixels_perc_paths = pd.read_sql_query(
    '''
    SELECT
        img_file0,
        img_file1,
        output_img_file,
        diff_pixels_perc
    FROM diff_stats
    ''',
    conn_diff,
)

# %%
diff_pixels_perc_paths.truncate(after=10)

# %% [markdown]
# Sampling bins:

# %% tags=[]
BIN_SAMPLE_FRAC = 0.13

screenshot_to_label_paths = pd.concat(
    [diff_pixels_perc_paths[(diff_pixels_perc_paths['diff_pixels_perc'] >= ps) & (diff_pixels_perc_paths['diff_pixels_perc'] < pe)] \
        .sample(frac=BIN_SAMPLE_FRAC, random_state=42) for ps, pe in zip(perc_bins, perc_bins[1:])]
    )
# Shuffle the samples
screenshot_to_label_paths = screenshot_to_label_paths \
    .sample(frac=1, random_state=42) \
    .reset_index(drop=True)
screenshot_to_label_paths = screenshot_to_label_paths.set_index('img_file0')

nojs_screenshot_paths = screenshot_paths[['js_screenshot_path', 'nojs_screenshot_path']] \
    .rename(columns={'js_screenshot_path': 'img_file0'})
nojs_screenshot_paths = nojs_screenshot_paths.set_index('img_file0')

screenshot_to_label_paths = screenshot_to_label_paths \
    .drop(columns=['diff_pixels_perc']) \
    .join(
        nojs_screenshot_paths,
        on='img_file0',
        how='left',
    ) \
    .reset_index(drop=False)

# Reorder the columns
screenshot_to_label_paths = screenshot_to_label_paths[['img_file0', 'nojs_screenshot_path', 'img_file1', 'output_img_file']]

screenshot_to_label_paths.to_csv('screenshots_to_label.tsv', sep='\t', header=False, index=False)

# %% [markdown]
# Count of screenshots to label:

# %%
screenshot_to_label_paths.shape[0]

# %% [markdown] tags=[] jp-MarkdownHeadingCollapsed=true
# ## Screenshot classification analysis

# %%
conn_labels = connect('../crawl_ubi-a_data/crawl_ubi-a_2/labels.db')

# %%
result_labels = pd.read_sql_query(
     '''
     SELECT *
     FROM labels;
     ''',
    conn_labels,
    )

# %% [markdown]
# Count of labeled pages:

# %%
LABEL_COUNT = result_labels.shape[0]
LABEL_COUNT

# %% [markdown]
# Counts of every possible labeling outcomes:

# %%
result_labels[['label', 'label2']].value_counts()

# %% [markdown]
# Percentages of every possible labeling outcomes:

# %%
all_label_perc = (100 * result_labels[['label', 'label2']].value_counts(normalize=True)).to_frame()
all_label_perc

# %% [markdown]
# Percentages of pages for each `label`:

# %%
label_perc = 100 * result_labels[['label', 'label2']].groupby('label').count() / LABEL_COUNT
label_perc

# %%
result_labels['label'].truncate(after=10)

# %%
grid = sns.catplot(
    data=result_labels,
    x='label',
    kind='count',
)
grid.ax.set(
    ylabel='Count',
    xlabel='Label',
);

# %%
grid = sns.JointGrid(
    data=result_labels,
    x='label2',
    y='label',
    marginal_ticks=True,
)
grid.set_axis_labels(xlabel='Label repair effect', ylabel="Label compliance with the 'read-only' UBI")
#cax = grid.figure.add_axes([.15, .55, .02, .5])
grid.plot_joint(
    sns.histplot,
    discrete=(True, True),
    cmap="light:#03012d",
    pmax=.8,
    #cbar=True,
    #cbar_ax=cax,
)
grid.plot_marginals(
    sns.histplot,
    element='step',
    color='#03012d',
);
plt.gcf().savefig('../paper-ubi-a/label_jointplot.pdf', bbox_inches='tight')

# %% [markdown]
# Removing the `0` label and re-normalizing:

# %%
label_perc_no_zero = label_perc.drop(index=[0]) 
label_perc_no_zero = 100 * label_perc_no_zero / label_perc_no_zero.sum()
label_perc_no_zero

# %% [markdown]
# Percentages of pages for each `label2`:

# %%
label2_perc = 100 * result_labels[['label', 'label2']].groupby('label2').count() / LABEL_COUNT
label2_perc

# %% [markdown]
# Removing the `0` label and re-normalizing:

# %%
label2_perc_no_zero = label2_perc.drop(index=[0]) 
label2_perc_no_zero = 100 * label2_perc_no_zero / label2_perc_no_zero.sum()
label2_perc_no_zero

# %% [markdown]
# Percentage of pages where the repairs lead to a worse outcome:

# %%
label2_perc_no_zero.loc[1][0]

# %% [markdown]
# Percentage of pages acceptable for the 'read-only' UBI when tolerating only minor information loss:

# %%
perc_pages_ok_minor_only = label_perc_no_zero.loc[4][0] + label_perc_no_zero.loc[5][0]
perc_pages_ok_minor_only

# %% [markdown]
# Percentage of pages acceptable for the 'read-only' UBI when tolerating the loss of some non-central sections:

# %%
perc_pages_ok_sec = label_perc_no_zero.loc[3][0] + label_perc_no_zero.loc[4][0] + label_perc_no_zero.loc[5][0]
perc_pages_ok_sec

# %% [markdown]
# Percentage of pages acceptable for the 'read-only' UBI when tolerating only minor information loss, without repairs:

# %%
perc_minor_only_wo_repairs = all_label_perc.loc[(5, 2), 0] + all_label_perc.loc[(4, 2), 0]
perc_minor_only_wo_repairs

# %% [markdown]
# Percentage of pages acceptable for the 'read-only' UBI when tolerating the loss of some non-central sections, without repairs:

# %%
perc_sec_wo_repairs = all_label_perc.loc[(5, 2), 0] + all_label_perc.loc[(4, 2), 0] + all_label_perc.loc[(3, 2), 0]
perc_sec_wo_repairs

# %% [markdown]
# Improvement (percentage of pages) brought by targeted repairs, when tolerating only minor information loss:

# %%
perc_improv_minor_only = 100 * (perc_pages_ok_minor_only - perc_minor_only_wo_repairs) / perc_minor_only_wo_repairs

# %% [markdown]
# Improvement (percentage of pages) brought by targeted repairs, when tolerating the loss of some non-central sections:

# %%
perc_improv_sec = 100 * (perc_pages_ok_sec - perc_sec_wo_repairs) / perc_sec_wo_repairs

# %%
table_improv = pd.DataFrame(
    data={
        '\\nojslabel{} (\%)': [perc_minor_only_wo_repairs, perc_sec_wo_repairs],
        '\\nojsubilabel{} (\%)': [perc_pages_ok_minor_only, perc_pages_ok_sec],
        'Repair improvements (\%)': [perc_improv_minor_only, perc_improv_sec],
    },
    index=['Minor information loss (4) + (5)', 'Loss of some non-central sections (3) + (4) + (5)'],
    )
table_improv

# %%
table_improv.style.to_latex(
    '../paper-ubi-a/table_ubi_improvements.tex',
    position='t',
    environment='table*',
    column_format='rrrr',
    label='tab:ubi-improvements',
    caption='Shares of pages compliant with the \\readonlyubi{} and the indicated tolerance',
    )

# %%

# %%
