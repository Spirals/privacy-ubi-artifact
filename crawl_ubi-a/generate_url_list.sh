#!/usr/bin/env sh

set -o errexit
set -o nounset

# Obtain the Tranco domain list
wget 'https://tranco-list.eu/top-1m.csv.zip'
unzip -qq top-1m.csv.zip top-1m.csv

top_domain_count=5000
rand_domain_count=5000
rand_domain_max_rank=100000

top_domains="$(head -n"${top_domain_count}" top-1m.csv)"

# Pick a random sample of the other domains
rand_domains="$(head -n"${rand_domain_max_rank}" top-1m.csv \
  | tail -n+"${top_domain_count}" \
  | shuf \
  | head -n"${rand_domain_count}")"

final_domain_lines="$(printf '%s\n%s' "${top_domains}" "${rand_domains}")"

# Turn domains into proper URLs
url_lines="$(echo "${final_domain_lines}" | sed -e 's|,|,http://|' -e 's/\r$//')"

echo "${url_lines}"

rm top-1m.csv.zip top-1m.csv
