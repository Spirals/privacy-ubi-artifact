# `image-labeling`

> An web interface to label doubles or triples of images.

Help can be obtained with:

```sh
cargo run -- --help
```

An example of usage can be found in the `Dockerfile`.

## Security

This tool must only be used on data you trust, no data validation is currently implemented.

## License

Licensed under the Apache-2.0 license (see LICENSE).

Copyright (c) 2022 Inria
