use std::collections::HashMap;
use std::fs::File;
use std::io::{self, BufRead};
use std::path::{Path, PathBuf};

use actix_web::Result;

use crate::ClassifyMode;

#[derive(Debug)]
pub enum ParseInputError {
    Io(io::Error),
    Line(usize),
}

impl From<io::Error> for ParseInputError {
    fn from(err: io::Error) -> Self {
        Self::Io(err)
    }
}

impl std::fmt::Display for ParseInputError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match *self {
            Self::Io(ref err) => err.fmt(f),
            Self::Line(line_no) => write!(f, "error while parsing line {line_no}"),
        }
    }
}

fn try_get_field<'a>(
    line_no: usize,
    fields: &mut impl Iterator<Item = &'a str>,
) -> Result<String, ParseInputError> {
    Ok(fields
        .next()
        .ok_or(ParseInputError::Line(line_no))?
        .to_string())
}

pub fn parse_input_file(
    path: &Path,
    classify_mode: &ClassifyMode,
) -> Result<Vec<[Option<String>; 6]>, ParseInputError> {
    let input_file = File::open(path)?;
    io::BufReader::new(input_file)
        .lines()
        .enumerate()
        .map(|(line_no, line)| {
            let line = line.unwrap();
            let mut fields = line.split(|c| c == '\t');
            Ok(match classify_mode {
                ClassifyMode::Single => [
                    Some(try_get_field(line_no, &mut fields)?),
                    None,
                    None,
                    None,
                    None,
                    None,
                ],
                ClassifyMode::Three => [
                    Some(try_get_field(line_no, &mut fields)?),
                    Some(try_get_field(line_no, &mut fields)?),
                    Some(try_get_field(line_no, &mut fields)?),
                    None,
                    None,
                    None,
                ],
                ClassifyMode::Four => [
                    Some(try_get_field(line_no, &mut fields)?),
                    Some(try_get_field(line_no, &mut fields)?),
                    Some(try_get_field(line_no, &mut fields)?),
                    Some(try_get_field(line_no, &mut fields)?),
                    None,
                    None,
                ],
            })
        })
        .collect::<Result<Vec<_>, ParseInputError>>()
}

#[derive(Debug)]
pub enum ParseLabelMappingError {
    Io { path: PathBuf, source: io::Error },
    Line(usize),
}

impl std::fmt::Display for ParseLabelMappingError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match *self {
            Self::Io {
                ref path,
                ref source,
            } => {
                write!(f, "IO error on file {}: {source}", path.to_string_lossy())
            }
            Self::Line(line_no) => write!(f, "error while parsing line {line_no}"),
        }
    }
}

pub fn parse_label_mapping(
    label_mapping_filepath: &Path,
) -> Result<HashMap<String, String>, ParseLabelMappingError> {
    io::BufReader::new(File::open(label_mapping_filepath).map_err(|err| {
        ParseLabelMappingError::Io {
            path: label_mapping_filepath.to_path_buf(),
            source: err,
        }
    })?)
    .lines()
    .enumerate()
    .map(|(i, line)| {
        let fields = line.unwrap();
        let mut fields = fields.split(|c| c == '\t');
        Ok((
            fields
                .next()
                .ok_or(ParseLabelMappingError::Line(i))?
                .to_string(),
            fields
                .next()
                .ok_or(ParseLabelMappingError::Line(i))?
                .to_string(),
        ))
    })
    .collect::<Result<_, ParseLabelMappingError>>()
}
