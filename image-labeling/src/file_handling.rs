use std::fs;
use std::path::Path;

pub fn move_image_to_dir(image_src: &Path, label_dir: &Path) {
    fs::create_dir_all(&label_dir).unwrap_or_else(|err| {
        panic!(
            "Could not create the label directory {} in the output directory: {err}",
            label_dir.to_string_lossy(),
        )
    });

    let to_filepath = label_dir.join(Path::new(&image_src).file_name().unwrap());
    fs::rename(&image_src, &to_filepath).unwrap_or_else(|err| {
        panic!(
            "Could not move the image {} to the label directory {}: {err}",
            image_src.to_string_lossy(),
            to_filepath.to_string_lossy(),
        )
    });
}
