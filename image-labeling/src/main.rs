#![forbid(unsafe_code)]

use std::collections::HashMap;
use std::path::PathBuf;

use actix_web::http::ContentEncoding;
use actix_web::{middleware, web, App, HttpServer};
use clap::Parser;
use log::debug;
use log::info;

mod file_handling;
mod input_file_parsing;
mod routes;

#[derive(Debug)]
pub struct AppState {
    input_lines: Vec<[Option<String>; 6]>,
    output_dir: PathBuf,
    label_mapping: HashMap<String, String>,
    classify_mode: ClassifyMode,
    move_image: bool,
    second_label: bool,
}

#[derive(Debug)]
pub enum ClassifyMode {
    Single,
    Three,
    Four,
}

#[derive(Parser)]
/// Classify images manually using a web-based GUI.
/// It can be used to classify either single images or triples of images.
/// It produces a JSON file per classification and can move the image into directories based on the
/// assigned label.
#[clap(name = "Image labeling")]
struct Args {
    /// Classify single images: provide the path of a file containing one filepath on each line
    #[clap(long, required_unless_present_any = ["classify-triples", "classify-four"])]
    classify_single: Option<PathBuf>,

    /// Classify image triples: provide the path of a TSV file containing three filepaths on each
    /// line
    #[clap(long, conflicts_with = "classify-four")]
    classify_triples: Option<PathBuf>,

    /// Classify four images: provide the path of a TSV file containing four filepaths on each line
    #[clap(long)]
    classify_four: Option<PathBuf>,

    /// Move the image into a label directory in `OUTPUT_DIR'
    #[clap(long, requires = "classify-single")]
    move_image: bool,

    /// TSV file describing the label mapping: on each line, a number between 1 and 9 followed by
    /// a label to apply
    #[clap(long, parse(from_os_str))]
    label_mapping_file: Option<PathBuf>,

    /// Directory for output files
    #[clap(short, parse(from_os_str))]
    output_dir: PathBuf,

    /// Listening port
    #[clap(long, short = 'p', default_value_t = 8080, parse(try_from_str))]
    listening_port: u16,

    /// Add a second label input
    #[clap(long)]
    second_label: bool,
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    env_logger::init();

    let args = Args::parse();

    assert!(args.output_dir.is_dir(), "OUTPUT_DIR must be a directory.");

    let (classify_mode, input_filepath) = if let Some(ref path) = args.classify_single {
        (ClassifyMode::Single, path)
    } else if let Some(ref path) = args.classify_triples {
        (ClassifyMode::Three, path)
    } else if let Some(ref path) = args.classify_four {
        (ClassifyMode::Four, path)
    } else {
        unreachable!();
    };
    debug!("Selected input file {}", input_filepath.to_string_lossy());
    let input_lines = input_file_parsing::parse_input_file(input_filepath, &classify_mode)
        .unwrap_or_else(|err| panic!("Could not parse the input file: {err}"));

    let label_mapping = match args.label_mapping_file {
        Some(path) => input_file_parsing::parse_label_mapping(&path)
            .unwrap_or_else(|err| panic!("Could not parse the label mapping file: {err}")),
        _ => HashMap::new(),
    };

    let data = web::Data::new(AppState {
        input_lines,
        output_dir: args.output_dir,
        label_mapping,
        classify_mode,
        move_image: args.move_image,
        second_label: args.second_label,
    });

    let server_address = format!("0.0.0.0:{}", args.listening_port);
    info!("Listening on http://{}", server_address);

    HttpServer::new(move || {
        App::new()
            .app_data(data.clone())
            .wrap(middleware::Compress::new(ContentEncoding::Gzip))
            .service(routes::image::image)
            .service(routes::static_asset::static_asset)
            .service(routes::label::label)
            .service(routes::skip::skip)
            .service(routes::images::images)
            .service(routes::index::index)
    })
    .bind(server_address)?
    .run()
    .await
}
