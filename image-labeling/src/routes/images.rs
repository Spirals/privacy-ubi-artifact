use std::str::FromStr;

use actix_web::{get, web, HttpResponse, Responder};
use askama::Template;
use log::info;

use crate::{AppState, ClassifyMode};

pub type Id = u64;

pub fn get_next_image_id(id: Id) -> Id {
    id + 1
}

fn encode_image_path(path: &str) -> String {
    base64::encode_config(
        path.as_bytes(),
        base64::Config::new(base64::CharacterSet::UrlSafe, false),
    )
    .to_string()
}

#[derive(Template)]
#[template(path = "single_image.html")]
struct LabelSingleImageTemplate<'a> {
    id: Id,
    image_src: &'a str,
    original_image_src: &'a str,
    image_page_url: Option<&'a str>,
    label_mapping: Vec<(String, String)>,
    second_label: bool,
}

#[derive(Template)]
#[template(path = "three_images.html")]
struct LabelImageTripleTemplate<'a> {
    id: Id,
    before_image_src: &'a str,
    after_image_src: &'a str,
    diff_image_src: &'a str,
    diff_perc: Option<f32>,
    image_page_url: Option<&'a str>,
    label_mapping: Vec<(String, String)>,
    second_label: bool,
}

#[derive(Template)]
#[template(path = "four_images.html")]
struct LabelImageFourTemplate<'a> {
    id: Id,
    before_image_src: &'a str,
    ref_image_src: &'a str,
    after_image_src: &'a str,
    diff_image_src: &'a str,
    diff_perc: Option<f32>,
    image_page_url: Option<&'a str>,
    label_mapping: Vec<(String, String)>,
    second_label: bool,
}

#[derive(Template)]
#[template(path = "done.html")]
struct DoneTemplate {}

#[get("/{id}")]
pub async fn images(data: web::Data<AppState>, info: web::Path<Id>) -> impl Responder {
    let id = info.into_inner();

    let second_label = data.second_label;

    info!("Serving id {id}");

    let mut label_mapping = data.label_mapping.clone().drain().collect::<Vec<_>>();
    label_mapping.sort_by_key(|(k, _)| k.clone());

    if let Some(line) = data.input_lines.get(id as usize) {
        let label_page = match data.classify_mode {
            ClassifyMode::Single => {
                let label_page = LabelSingleImageTemplate {
                    id,
                    image_src: &format!("/images/{}", encode_image_path(line[0].as_ref().unwrap())),
                    original_image_src: line[0].as_ref().unwrap(),
                    image_page_url: None,
                    label_mapping,
                    second_label,
                };
                info!("Serving page for {}", line[0].as_ref().unwrap());
                label_page.render()
            }
            ClassifyMode::Three => {
                let label_page = LabelImageTripleTemplate {
                    id,
                    before_image_src: &format!(
                        "/images/{}",
                        encode_image_path(line[0].as_ref().unwrap())
                    ),
                    after_image_src: &format!(
                        "/images/{}",
                        encode_image_path(line[1].as_ref().unwrap())
                    ),
                    diff_image_src: &format!(
                        "/images/{}",
                        encode_image_path(line[2].as_ref().unwrap())
                    ),
                    diff_perc: line[3].as_ref().map(|s| f32::from_str(s).unwrap()),
                    image_page_url: line[4].as_deref(),
                    label_mapping,
                    second_label,
                };
                info!(
                    "Serving page for {}\n{}\n{}",
                    line[0].as_ref().unwrap(),
                    line[1].as_ref().unwrap(),
                    line[2].as_ref().unwrap(),
                );
                label_page.render()
            }
            ClassifyMode::Four => {
                let label_page = LabelImageFourTemplate {
                    id,
                    before_image_src: &format!(
                        "/images/{}",
                        encode_image_path(line[0].as_ref().unwrap())
                    ),
                    ref_image_src: &format!(
                        "/images/{}",
                        encode_image_path(line[1].as_ref().unwrap())
                    ),
                    after_image_src: &format!(
                        "/images/{}",
                        encode_image_path(line[2].as_ref().unwrap())
                    ),
                    diff_image_src: &format!(
                        "/images/{}",
                        encode_image_path(line[3].as_ref().unwrap())
                    ),
                    diff_perc: line[4].as_ref().map(|s| f32::from_str(s).unwrap()),
                    image_page_url: line[5].as_deref(),
                    label_mapping,
                    second_label,
                };
                info!(
                    "Serving page for {}\n{}\n{}\n{}",
                    line[0].as_ref().unwrap(),
                    line[1].as_ref().unwrap(),
                    line[2].as_ref().unwrap(),
                    line[3].as_ref().unwrap(),
                );
                label_page.render()
            }
        };

        let mut label_mapping = data.label_mapping.clone().drain().collect::<Vec<_>>();
        label_mapping.sort_by_key(|(k, _)| k.clone());

        HttpResponse::Ok().body(label_page.unwrap())
    } else {
        let done_page = DoneTemplate {};
        HttpResponse::Ok().body(done_page.render().unwrap())
    }
}
