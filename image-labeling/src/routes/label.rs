use std::fs;
use std::path::{Path, PathBuf};
use std::str::FromStr;

use actix_web::{post, web, HttpResponse, Responder};
use log::{debug, info};
use serde::{Deserialize, Serialize};

use super::images::{get_next_image_id, Id};
use crate::file_handling::move_image_to_dir;
use crate::{AppState, ClassifyMode};

type Label = u8;

#[derive(Deserialize)]
pub struct LabelForm {
    id: Id,
    label: Label,
    label2: Option<Label>,
    original_image_src: Option<String>,
}

#[derive(Serialize)]
struct LabelSingleImageRecord<'a> {
    id: u64,
    img: &'a str,
    label: Label,
    label2: Option<Label>,
}

#[derive(Serialize)]
struct LabelImageTripleRecord<'a> {
    id: u64,
    before_img: &'a str,
    after_img: &'a str,
    diff_img: &'a str,
    label: Label,
    label2: Option<Label>,
    diff_pixels_perc: Option<f32>,
    url: Option<&'a str>,
}

#[derive(Serialize)]
struct LabelImageFourRecord<'a> {
    id: u64,
    before_img: &'a str,
    ref_img: &'a str,
    after_img: &'a str,
    diff_img: &'a str,
    label: Label,
    label2: Option<Label>,
    diff_pixels_perc: Option<f32>,
    url: Option<&'a str>,
}

#[post("/label")]
pub async fn label(data: web::Data<AppState>, form: web::Form<LabelForm>) -> impl Responder {
    let id = form.id;

    if let Some(line) = data.input_lines.get(id as usize) {
        info!("Label received: id {id}");

        debug!("Input line: {line:?}");

        let label = form.label;
        let label2 = form.label2;

        let label_record = match data.classify_mode {
            ClassifyMode::Single => serde_json::to_string(&LabelSingleImageRecord {
                id,
                img: form.original_image_src.as_ref().unwrap(),
                label,
                label2,
            }),
            ClassifyMode::Three => serde_json::to_string(&LabelImageTripleRecord {
                id,
                before_img: line[0].as_ref().unwrap(),
                after_img: line[1].as_ref().unwrap(),
                diff_img: line[2].as_ref().unwrap(),
                label,
                label2,
                diff_pixels_perc: line
                    .get(3)
                    .map(|s| f32::from_str(s.as_ref().unwrap()).unwrap()),
                url: line.get(4).map(|s| s.as_ref().unwrap().as_str()),
            }),
            ClassifyMode::Four => serde_json::to_string(&LabelImageFourRecord {
                id,
                before_img: line[0].as_ref().unwrap(),
                ref_img: line[1].as_ref().unwrap(),
                after_img: line[2].as_ref().unwrap(),
                diff_img: line[3].as_ref().unwrap(),
                label,
                label2,
                diff_pixels_perc: line[4].as_ref().map(|s| f32::from_str(s).unwrap()),
                url: line[5].as_deref(),
            }),
        };

        let json_output = label_record.unwrap();

        debug!("JSON output: {json_output}");

        let filename = data
            .output_dir
            .join(Path::new(&format!("image_label_{}.json", id)));
        fs::write(&filename, json_output).unwrap();

        info!("JSON printed to {}", filename.to_str().unwrap());

        if data.move_image {
            let label = label.to_string();
            let label_dir = data
                .output_dir
                .join(data.label_mapping.get(&label).or(Some(&label)).unwrap());

            move_image_to_dir(
                &PathBuf::from(&form.original_image_src.as_ref().unwrap()),
                &label_dir,
            );
        }

        HttpResponse::SeeOther()
            .header("Location", format!("/{}", get_next_image_id(id)))
            .keep_alive()
            .finish()
    } else {
        HttpResponse::BadRequest().finish()
    }
}
