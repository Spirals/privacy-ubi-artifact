use actix_web::{get, HttpResponse, Responder};

#[get("/")]
async fn index() -> impl Responder {
    HttpResponse::TemporaryRedirect()
        .header("Location", format!("/{}", 0))
        .keep_alive()
        .finish()
}
