use actix_web::{get, web, HttpResponse, Responder};

#[derive(rust_embed::RustEmbed)]
#[folder = "static"]
struct Asset;

fn handle_embedded_file(path: &str) -> HttpResponse {
    match Asset::get(path) {
        Some(content) => HttpResponse::Ok()
            .content_type(mime_guess::from_path(path).first_or_octet_stream().as_ref())
            .body(content.data.into_owned()),
        None => HttpResponse::NotFound().body("404 Not Found"),
    }
}

#[get("/static/{_:.*}")]
async fn static_asset(path: web::Path<String>) -> impl Responder {
    handle_embedded_file(path.as_str())
}
