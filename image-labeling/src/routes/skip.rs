use std::path::PathBuf;

use actix_web::{post, web, HttpResponse, Responder};
use serde::Deserialize;

use super::images::{get_next_image_id, Id};
use crate::file_handling::move_image_to_dir;
use crate::AppState;

#[derive(Deserialize)]
pub struct SkipForm {
    id: Id,
    original_image_src: Option<String>,
}

#[post("/skip")]
pub async fn skip(data: web::Data<AppState>, form: web::Form<SkipForm>) -> impl Responder {
    if data.move_image {
        move_image_to_dir(
            &PathBuf::from(&form.original_image_src.as_ref().unwrap()),
            &data.output_dir.join("skipped"),
        );
    }

    HttpResponse::SeeOther()
        .header("Location", format!("/{}", get_next_image_id(form.id)))
        .keep_alive()
        .finish()
}
