use actix_files as actix_fs;
use actix_web::http::header::{ContentDisposition, DispositionType};
use actix_web::{get, HttpRequest, Result};
use log::debug;

// Image needs special handling because some image filepaths contain question marks, which is
// removed by the routing mechanism
#[get("/images/{filename:.*}")]
pub async fn image(req: HttpRequest) -> Result<actix_fs::NamedFile> {
    let mut path = req
        .match_info()
        .query("filename")
        .parse::<String>()
        .unwrap();
    if !req.query_string().is_empty() {
        // Remove the trailing slash of the path added by the browser
        path.pop();
        path += "?";
    }

    let image_path = String::from_utf8(
        base64::decode_config(
            &path,
            base64::Config::new(base64::CharacterSet::UrlSafe, false),
        )
        .unwrap(),
    )
    .unwrap();
    let image_file = actix_fs::NamedFile::open(&image_path);

    if let Err(ref err) = image_file {
        debug!("Error while serving image {image_path}: {err}");
    } else {
        debug!("Serving image {image_path}");
    }

    // Discard the filename from the Content-Disposition header value as it could cause encoding
    // issues
    let image_file = image_file?.set_content_disposition(ContentDisposition {
        disposition: DispositionType::Inline,
        parameters: Vec::new(),
    });

    Ok(image_file)
}
