# `crawl_framework_rs`

This crate contains generic crawl helpers which are used in the crawler.

## License

Licensed under the Apache-2.0 license (see LICENSE).

Copyright (c) 2022 Inria
