//! A glorified async CondVar to wait for tabs for their network to become idle.

use std::collections::HashSet;
use std::time::{Duration, Instant};

use log::debug;
use thirtyfour::session::handle::SessionHandle;
use thirtyfour::By;
use thirtyfour::WindowHandle;

use crate::{CrawlError, CrawlResult};

static NETWORK_IDLE_TIMEOUT: u8 = 5;

/// UUID associated with a browser tab.
/// It is injected into pages by a WebExtension (e.g., `network-idle-webext`).
#[derive(Debug, PartialEq, Eq, Hash)]
pub struct TabUuid(pub String);

impl std::fmt::Display for TabUuid {
    #[inline]
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

/// Message to send in the [crossbeam_channel] to notify [TabIdleWaiter] that a tab is now idle.
///
/// See [`TabIdleWaiter::new()`] for an example.
#[derive(Debug)]
pub struct TabNetworkIdle(pub TabUuid);

/// A glorified async CondVar to wait for tabs for their network to become idle.
///
/// It requires the `network-idle-webext` WebExtension to be installed in the browser.
/// This extension has two jobs: (1) injecting a unique tab UUID in each page, to identify each tab
/// uniquely, and (2) monitoring network requests and sending an HTTP `POST` request to a web
/// server that communicates with [TabIdleWaiter] when the tab had had no requests in-flight for a
/// few seconds (configurable in the WebExtension).
#[derive(Debug)]
pub struct TabIdleWaiter {
    idle_tabs: tokio::sync::RwLock<HashSet<TabUuid>>,
    receiver: crossbeam_channel::Receiver<TabNetworkIdle>,
    polling_delay: Duration,
}

impl TabIdleWaiter {
    /// Create a [TabIdleWaiter]. It needs to be provided with the receiver end of a
    /// [`crossbeam_channel`] to receive notifications of tab becoming idle, sent by the
    /// `network-idle-webext` WebExtension.
    ///
    /// ```
    /// # use std::collections::HashMap;
    /// # use std::sync::Arc;
    /// use std::convert::Infallible;
    ///
    /// use hyper::{Body, Method, Request, Response};
    ///
    /// use crawl_framework::tab_idle_waiter::{self, TabIdleWaiter};
    ///
    /// async fn route(
    ///     net_idle_sender: crossbeam_channel::Sender<tab_idle_waiter::TabNetworkIdle>,
    ///     req: Request<Body>,
    /// ) -> Result<Response<Body>, Infallible> {
    ///     match (req.method(), req.uri().path()) {
    ///         (&Method::POST, "/networkidle") => {
    ///             let params =
    ///                 url::form_urlencoded::parse(hyper::body::to_bytes(req).await.unwrap().as_ref())
    ///                     .into_owned()
    ///                     .collect::<HashMap<String, String>>();
    ///             net_idle_sender
    ///                 .send(tab_idle_waiter::TabNetworkIdle(
    ///                     tab_idle_waiter::TabUuid(params.get("tabUuid").unwrap().to_string()),
    ///                 ))
    ///                 .unwrap();
    ///            Ok(Response::default())
    ///         }
    ///         _ => unreachable!(),
    ///     }
    /// }
    ///
    /// let (net_idle_sender, net_idle_receiver) = crossbeam_channel::unbounded();
    /// let tab_idle_waiter = Arc::new(tokio::sync::Mutex::new(TabIdleWaiter::new(
    ///     net_idle_receiver.clone(),
    /// )));
    /// ```
    pub fn new(receiver: crossbeam_channel::Receiver<TabNetworkIdle>) -> Self {
        Self {
            idle_tabs: tokio::sync::RwLock::new(HashSet::new()),
            receiver,
            polling_delay: Duration::from_millis(100),
        }
    }

    async fn mark_tab_idle(&self, uuid: TabUuid) {
        debug!("Tab network idle for {NETWORK_IDLE_TIMEOUT}\u{a0}s: {uuid}");
        let mut idle_tabs_guard = self.idle_tabs.write().await;
        idle_tabs_guard.insert(uuid);
    }

    /// Get the [`TabUuid`] associated to a tab.
    /// This [`TabUuid`] must have been injected by a WebExtension (e.g., `network-idle-webext`).
    ///
    /// # Errors
    /// Will return [`CrawlError::WebDriverError`] if an error happens when communicating with the
    /// browser.
    ///
    /// Will return [`CrawlError::TabUuidNotFound`] if the tab cannot be found.
    pub async fn get_tab_uuid(
        session_guard: &tokio::sync::MutexGuard<'_, SessionHandle>,
        window_handle: &WindowHandle,
    ) -> CrawlResult<TabUuid> {
        session_guard
            .switch_to()
            .window(window_handle.clone())
            .await?;

        if let Ok(el) = session_guard.find_element(By::Id("tab-uuid")).await {
            return Ok(el.inner_html().await.map(TabUuid)?);
        }

        Err(CrawlError::TabUuidNotFound)
    }

    /// Same as [`TabIdleWaiter::get_tab_uuid()`], but wait for the [`TabUuid`] to be found, with a
    /// timeout in case it is not.
    ///
    /// # Errors
    /// Will return [`CrawlError::WebDriverError`] if an error happens when communicating with the
    /// browser.
    ///
    /// Will return [`CrawlError::TabUuidNotFound`] if the tab cannot be found.
    pub async fn get_tab_uuid_with_timeout(
        session_guard: &tokio::sync::MutexGuard<'_, SessionHandle>,
        window_handle: &WindowHandle,
        timeout: Duration,
    ) -> CrawlResult<TabUuid> {
        let waiting_start = Instant::now();

        loop {
            if let Ok(tab_uuid) = Self::get_tab_uuid(session_guard, window_handle).await {
                return Ok(tab_uuid);
            }

            if waiting_start.elapsed() > timeout {
                return Err(CrawlError::TabUuidNotFound);
            }
        }
    }

    async fn is_tab_idle(
        &self,
        session: &tokio::sync::Mutex<SessionHandle>,
        window_handle: &WindowHandle,
    ) -> CrawlResult<bool> {
        let tab_uuid: CrawlResult<TabUuid> = {
            let session_guard = session.lock().await;
            session_guard
                .switch_to()
                .window(window_handle.clone())
                .await?;

            Self::get_tab_uuid(&session_guard, window_handle).await
        };

        let idle_tabs_guard = self.idle_tabs.read().await;
        tab_uuid.map(|uuid| idle_tabs_guard.contains(&uuid))
    }

    /// Wait until this tab has no in-flight requests for a few seconds. The actual network-idle
    /// duration is defined in the WebExtension `network-idle-webext`.
    ///
    /// Returns true if the tab has been found to be idle, false if we ran out of time waiting.
    ///
    /// # Errors
    /// Will return [`CrawlError::WebDriverError`] if an error happens when communicating with the
    /// browser.
    ///
    /// Will return [`CrawlError::ChannelReadError`] if an error happens when reading the
    /// communication channel.
    pub async fn wait_for_tab_idle_with_timeout(
        &self,
        session: &tokio::sync::Mutex<SessionHandle>,
        window_handle: &WindowHandle,
        timeout: Duration,
    ) -> CrawlResult<bool> {
        use crossbeam_channel::TryRecvError;

        let waiting_start = Instant::now();

        loop {
            // Read all available messages
            loop {
                match self.receiver.try_recv() {
                    Ok(TabNetworkIdle(uuid)) => self.mark_tab_idle(uuid).await,
                    Err(TryRecvError::Empty) => break,
                    Err(TryRecvError::Disconnected) => {
                        return Err(CrawlError::ChannelReadError(TryRecvError::Disconnected));
                    }
                }
            }

            if waiting_start.elapsed() > timeout {
                // Timeout
                return Ok(false);
            }

            if let Ok(true) = self.is_tab_idle(session, window_handle).await {
                return Ok(true);
            }

            tokio::time::sleep(self.polling_delay).await;
        }
    }

    /// Wait until this tab reaches the `complete`
    /// [`readyState`](https://developer.mozilla.org/en-US/docs/Web/API/Document/readyState).
    ///
    /// # Errors
    /// Will return [`CrawlError::WebDriverError`] if an error happens when communicating with the
    /// browser.
    ///
    /// Will return [`CrawlError::TimeoutPageComplete`] if we ran out of time waiting.
    pub async fn wait_for_complete_state(
        &self,
        session: &tokio::sync::Mutex<SessionHandle>,
        window_handle: WindowHandle,
        timeout: Duration,
    ) -> CrawlResult<()> {
        let session_guard = session.lock().await;
        session_guard
            .switch_to()
            .window(window_handle.clone())
            .await?;

        let waiting_start = Instant::now();

        loop {
            let ready_state: String = session_guard
                .execute_script(r#"return document.readyState"#, Vec::new())
                .await?
                .convert()?;

            if ready_state == "complete" {
                return Ok(());
            }

            if waiting_start.elapsed() > timeout {
                return Err(CrawlError::TimeoutPageComplete);
            }

            tokio::time::sleep(self.polling_delay).await;
        }
    }
}
