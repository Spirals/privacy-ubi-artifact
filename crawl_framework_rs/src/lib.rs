#![forbid(unsafe_code)]
// Documentation
#![deny(
    missing_docs,
    clippy::missing_errors_doc,
    rustdoc::broken_intra_doc_links,
    rustdoc::private_intra_doc_links
)]
// Avoid panics
#![deny(
    clippy::unwrap_used,
    clippy::expect_used,
    clippy::panic,
    clippy::indexing_slicing,
    clippy::unwrap_in_result,
    clippy::unreachable,
    clippy::unimplemented
)]
// Correctness
#![warn(
    clippy::pedantic,
    clippy::clone_on_ref_ptr,
    clippy::non_ascii_literal,
    clippy::dbg_macro,
    clippy::map_err_ignore,
    clippy::use_debug,
    clippy::pattern_type_mismatch,
    clippy::map_err_ignore
)]
#![allow(clippy::doc_markdown, clippy::must_use_candidate)]
// Style
#![warn(
    clippy::use_self,
    clippy::useless_let_if_seq,
    clippy::verbose_file_reads
)]

//! A framework to help building web crawls with [thirtyfour].
//!
//! It is mostly a collection of helpers and may get smaller when/if some of it gets upstreamed at some
//! point.

use std::io;
use std::path::{Path, PathBuf};
use std::time;

use crossbeam_channel::TryRecvError;
use thirtyfour::error::WebDriverError;
use thirtyfour::extensions::firefox::FirefoxTools;
use thirtyfour::session::handle::SessionHandle;
use thirtyfour::WindowHandle;
use thiserror::Error;
use tokio::fs;

pub mod data_manager;
pub mod tab_idle_waiter;

pub use thirtyfour;

/// An error type for crawl errors.
#[derive(Debug, Error)]
pub enum CrawlError {
    /// Error originating from a WebDriver operation.
    #[error("WebDriver error encountered: {0}")]
    WebDriverError(#[from] WebDriverError),

    /// Error when attempting to install a WebExtension.
    #[error(
        "{0} is not a valid path for a WebExtension (it is either missing or not a directory)"
    )]
    WebExtPathError(io::Error),

    /// Error when attempting to read a script to inject.
    #[error("error when attempting to read {0} as a script to inject")]
    JsSnippetReadError(PathBuf),

    /// Error when attempting to open a new tab: the new tab opened could not be obtained.
    #[error("error when attempting to open a new tab: could not obtain the opened new tab")]
    NewTabError,

    /// Error when attempting to close a tab: could not switch back to tab 0.
    #[error("error when attempting to close a tab: could not switch back to tab\u{a0}0")]
    SwitchBackTab,

    /// Error when attempting to read from a communication channel.
    #[error("error when reading channel: {0}")]
    ChannelReadError(#[from] TryRecvError),

    /// Error when attempting to obtain the tab's UUID: UUID not found.
    #[error("error when attempting to obtain the tab's UUID")]
    TabUuidNotFound,

    /// Error when attempting to save the screenshot to disk.
    #[error("error when attempting to save the screenshot to {0}")]
    ScreenshotWriteError(#[from] io::Error),

    /// Timeout when waiting for a page to reach complete state.
    #[error("timeout when waiting for a page to reach complete state")]
    TimeoutPageComplete,
}

/// A specialized `Result` type for crawl errors.
pub type CrawlResult<T> = Result<T, CrawlError>;

/// Open a new browser tab and return its handle.
///
/// The opened tab can later be closed using [`close_tab`].
///
/// # Errors
/// Will return [`CrawlError::WebDriverError`] if an error happens when communicating with the
/// browser.
///
/// Will return [`CrawlError::NewTabError`] if the new tab opened cannot be obtained.
pub async fn open_new_tab(
    session: &tokio::sync::Mutex<SessionHandle>,
) -> CrawlResult<WindowHandle> {
    let session_guard = session.lock().await;

    let existing_handles = session_guard.window_handles().await?;

    session_guard
        .execute_script(
            r#"window.open("about:blank", target="_blank");"#,
            Vec::new(),
        )
        .await?;

    let mut new_handles = session_guard.window_handles().await?;
    new_handles.retain(|h| !existing_handles.contains(h));
    if new_handles.len() != 1 {
        return Err(CrawlError::NewTabError);
    }

    Ok(new_handles.get(0).ok_or(CrawlError::NewTabError)?.clone())
}

/// Close the browser tab/window referenced by the `window_handle` parameter.
///
/// # Errors
/// Will return [`CrawlError::WebDriverError`] if an error happens when communicating with the
/// browser.
///
/// Will return [`CrawlError::SwitchBackTab`] if an error happens when attempting to switch to
/// tab 0 after closing the tab.
pub async fn close_tab(
    session: &tokio::sync::Mutex<SessionHandle>,
    window_handle: WindowHandle,
) -> CrawlResult<()> {
    let session_guard = session.lock().await;

    session_guard.switch_to().window(window_handle).await?;
    session_guard
        .execute_script(r#"window.close();"#, Vec::new())
        .await?;

    let existing_handles = session_guard.window_handles().await?;
    session_guard
        .switch_to()
        .window(
            existing_handles
                .get(0)
                .ok_or(CrawlError::SwitchBackTab)?
                .clone(),
        )
        .await?;

    Ok(())
}

/// Replace characters forbidden by ext4 (only `/`) in strings, by the [ASCII Substitution
/// Character](<https://en.wikipedia.org/wiki/Substitute_character>).
///
/// ```
/// # use crawl_framework::escape_ext4_filepath;
/// assert_eq!(escape_ext4_filepath("https://www.rust-lang.org/"), "https:\x1a\x1awww.rust-lang.org\x1a");
/// ```
#[inline]
pub fn escape_ext4_filepath(path: &str) -> String {
    path.replace('/', "\x1a")
}

/// Return the number of milliseconds since the UNIX epoch.
///
/// # Panics
/// Panics if [`SystemTime`](std::time::SystemTime) is before UNIX epoch.
#[inline]
pub fn millis_since_epoch() -> u128 {
    let now = time::SystemTime::now();

    #[allow(clippy::expect_used)]
    now.duration_since(time::UNIX_EPOCH)
        .expect("SystemTime is before UNIX epoch")
        .as_millis()
}

/// Append a timestamp at the end of the provided string, separated by an underscore.
///
/// The timestamp is the number of milliseconds since the [UNIX epoch](std::time::UNIX_EPOCH).
///
/// # Panics
/// Panics if [`SystemTime`](std::time::SystemTime) is before UNIX epoch.
///
/// ```
/// # use crawl_framework::append_timestamp;
/// let filename = append_timestamp("https:\x1a\x1awww.rust-lang.org\x1a");
/// assert!(
///     filename.starts_with("https:\x1a\x1awww.rust-lang.org\x1a_")
///         && filename.as_bytes()[27..40].iter().all(u8::is_ascii_digit),
/// );
/// ```
pub fn append_timestamp(string: &str) -> String {
    format!("{}_{}", string, millis_since_epoch())
}

/// Scroll the page by up to 3 viewport heights.
///
/// # Errors
/// Will return [`CrawlError::WebDriverError`] if an error happens when communicating with the
/// browser.
///
/// Will return [`CrawlError::JsSnippetReadError`] if an error happens when reading an internal
/// JavaScript file required for scrolling the page.
pub async fn scroll_page(
    session: &tokio::sync::Mutex<SessionHandle>,
    window_handle: WindowHandle,
) -> CrawlResult<()> {
    const SNIPPET_PATH: &str = "scroll_page.js";

    #[derive(rust_embed::RustEmbed)]
    #[folder = "src/js-snippets/"]
    struct JsSnippets;

    let session_guard = session.lock().await;
    session_guard
        .switch_to()
        .window(window_handle.clone())
        .await?;
    session_guard
        .execute_script_async(
            &String::from_utf8(
                JsSnippets::get(SNIPPET_PATH)
                    .ok_or_else(|| CrawlError::JsSnippetReadError(PathBuf::from(SNIPPET_PATH)))?
                    .data
                    .into(),
            )
            .map_err(|_err| CrawlError::JsSnippetReadError(PathBuf::from(SNIPPET_PATH)))?,
            vec![],
        )
        .await?;

    Ok(())
}

/// Install a local, temporary
/// [WebExtension](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions) in
/// Firefox.
///
/// The given path will be canonicalized with [`tokio::fs::canonicalize`].
///
/// # Errors
/// Will return [`CrawlError::WebDriverError`] if an error happens when communicating with the
/// browser.
///
/// Will return [`CrawlError::WebDriverError`] if an error happens when canonicalizing `webext_path`.
pub async fn install_temp_local_webext(
    tools: &FirefoxTools,
    webext_path: &Path,
) -> CrawlResult<()> {
    Ok(tools
        .install_addon(
            &tokio::fs::canonicalize(webext_path)
                .await
                .map_err(CrawlError::WebExtPathError)?
                .to_string_lossy(),
            Some(true), // Load the extension as a temporary extension
        )
        .await?)
}

/// Save a PNG screenshot of the full-page (not the part within the viewport only) to disk.
///
/// # Errors
/// Will return [`CrawlError::WebDriverError`] if an error happens when communicating with the
/// browser.
///
/// Will return [`CrawlError::ScreenshotWriteError`] if an error happens when attempting to write
/// the screenshot to disk.
pub async fn save_page_screenshot<P: AsRef<Path>>(
    session: &tokio::sync::Mutex<SessionHandle>,
    window_handle: WindowHandle,
    tools: &FirefoxTools,
    screenshot_path: P,
) -> CrawlResult<Vec<u8>> {
    let screenshot_path = screenshot_path.as_ref();

    let session_guard = session.lock().await;
    session_guard
        .switch_to()
        .window(window_handle.clone())
        .await?;

    let png_screenshot = tools.full_screenshot_as_png().await?;

    fs::write(screenshot_path, &png_screenshot)
        .await
        .map_err(CrawlError::ScreenshotWriteError)?;

    Ok(png_screenshot)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_escape_ext4_filepath() {
        assert_eq!(
            escape_ext4_filepath("https://www.rust-lang.org/"),
            "https:\x1a\x1awww.rust-lang.org\x1a",
        );
        assert_eq!(escape_ext4_filepath(""), "");
        assert_eq!(escape_ext4_filepath("/"), "\x1a");
    }

    #[test]
    fn test_append_timestamp() {
        let filename = append_timestamp("");
        assert!(
            filename.starts_with("_") && filename.as_bytes()[1..14].iter().all(u8::is_ascii_digit),
        );
    }
}
