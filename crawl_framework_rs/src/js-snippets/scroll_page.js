'use strict';
const done = arguments[0];
let totalScrolledHeight = 0;
const SCROLL_DISTANCE = 300;
let scrolls = 0;
const VIEWPORT_HEIGHT_SCROLL_COUNT = 3;
const timer = setInterval(() => {
    const currentPageHeight = document.body.scrollHeight;
    window.scrollBy(0, SCROLL_DISTANCE);
    scrolls++;
    totalScrolledHeight += SCROLL_DISTANCE;
    if(scrolls * SCROLL_DISTANCE >= VIEWPORT_HEIGHT_SCROLL_COUNT * window.innerHeight
        || totalScrolledHeight >= currentPageHeight
        || scrolls > 50) {
        clearInterval(timer);
        done(totalScrolledHeight);
    }
}, 100);
