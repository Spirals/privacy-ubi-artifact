//! A centralized manager to store JSON crawl data into a [SQLite](<https://www.sqlite.org>)
//! database.

use std::time::Duration;

use serde_json::json;
use sqlx::sqlite::SqliteConnectOptions;
use sqlx::{ConnectOptions, SqliteConnection};

use crate::tab_idle_waiter::TabUuid;

pub use sqlx;

/// Represents some timing data about a page, e.g., page load time.
#[derive(Debug, Clone)]
pub struct PageTiming {
    /// Page timing name
    pub name: String,
    /// Page timing duration
    pub duration: Duration,
}

impl std::fmt::Display for PageTiming {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            serde_json::to_string(&json!({
                self.name.clone():
                    u64::try_from(self.duration.as_millis()).map_err(|_err| std::fmt::Error)?,
            }))
            .map_err(|_err| std::fmt::Error)?
        )
    }
}

/// A centralized manager to store JSON crawl data into a [SQLite](<https://www.sqlite.org>)
/// database.
///
/// It should be wrapped in an `Arc<_>` when used in an async setup but does not need a Mutex, as
/// shown below, where `tab_uuid` is usually obtained with
/// [`TabIdleWaiter::get_tab_uuid_with_timeout()`](crate::tab_idle_waiter::TabIdleWaiter::get_tab_uuid_with_timeout):
///
/// ```
/// # use std::sync::Arc;
/// use crawl_framework::data_manager::DataManager;
/// use crawl_framework::tab_idle_waiter::TabUuid;
///
/// # #[tokio::main]
/// # async fn main() {
/// # let tmp_dir = tempdir::TempDir::new("").unwrap();
/// # std::env::set_current_dir(&tmp_dir).unwrap();
/// let data_manager = Arc::new(DataManager::new("crawl_data.db").await.unwrap());
///
/// // When the page has been loaded and the tab UUID has been obtained
/// let tab_uuid = TabUuid("6c3975e6-e72d-11ec-9c55-47e28fe513de".to_string());
/// let data_manager_handle = Arc::clone(&data_manager);
/// data_manager_handle.record_tab(
///     "https://rust-lang.org/",
///     "https://rust-lang.org/index.html",
///     42,
///     true,
///     &tab_uuid,
///     "js",
/// );
///
/// // When the tab crawl data has been obtained
/// data_manager_handle.record_data(
///     &tab_uuid,
///     &serde_json::from_str("{}").unwrap(), // JSON crawl data
/// );
/// # }
/// ```
#[derive(Debug)]
pub struct DataManager {
    sqlite_conn: tokio::sync::Mutex<SqliteConnection>,
}

impl DataManager {
    /// Create a new [DataManager] and specify where the SQLite database will be stored.
    ///
    /// # Errors
    /// Will return [`sqlx::Error`] if an error happens when creating the database table.
    pub async fn new(db_path: &str) -> Result<Self, sqlx::Error> {
        let mut sqlite_conn = SqliteConnectOptions::new()
            .filename(db_path)
            .create_if_missing(true)
            .connect()
            .await?;

        sqlx::query(
            "
            CREATE TABLE crawl_data (
                tab_uuid TEXT PRIMARY KEY,
                crawl_url TEXT,
                url TEXT,
                ranking INTEGER,
                landing_page INTEGER,
                crawl_type TEXT,
                page_timings TEXT DEFAULT '{}',
                screenshot_paths TEXT DEFAULT '{}',
                data TEXT DEFAULT '{}',
                error TEXT
            )
            ",
        )
        .execute(&mut sqlite_conn)
        .await?;

        Ok(Self {
            sqlite_conn: tokio::sync::Mutex::new(sqlite_conn),
        })
    }

    /// Record the association between a tuple (`crawl_url`, `url`, `ranking`, `landing_page`), a
    /// tab UUID and the `crawl_type`, as string identifying the browser instance when running
    /// crawls with multiple browser instances.
    ///
    /// # Errors
    /// Will return [`sqlx::Error`] if an error happens when upserting tab information.
    pub async fn record_tab(
        &self,
        crawl_url: &str,
        current_url: &str,
        ranking: u32,
        landing_page: bool,
        tab_uuid: &TabUuid,
        crawl_type: &str,
    ) -> sqlx::Result<()> {
        let mut sqlite_conn_guard = self.sqlite_conn.lock().await;

        sqlx::query(
            "
            INSERT INTO crawl_data (tab_uuid, crawl_url, url, ranking, landing_page, crawl_type)
            VALUES(?1, ?2, ?3, ?4, ?5, ?6)
            ON CONFLICT(tab_uuid)
            DO UPDATE SET crawl_url=?2, url=?3, ranking=?4, landing_page=?5, crawl_type=?6
            ",
        )
        .bind(tab_uuid.to_string())
        .bind(crawl_url)
        .bind(current_url)
        .bind(ranking)
        .bind(landing_page)
        .bind(crawl_type)
        .execute(&mut *sqlite_conn_guard)
        .await
        .map(|_| ())
    }

    /// Record the JSON data associated with a tab UUID.
    ///
    /// If some data has already been recorded for this tab, the new data will be merged using
    /// SQLite's [`json_patch`](https://www.sqlite.org/json1.html#jpatch) function.
    /// Use [`DataManager::record_tab()`] to associate this data to a tuple (`crawl_url`, `url`,
    /// `ranking`, `landing_page`); [`DataManager::record_tab()`] does not necessarily have to be
    /// called before this function.
    ///
    /// # Errors
    /// Will return [`sqlx::Error`] if an error happens when upserting data.
    pub async fn record_data(
        &self,
        tab_uuid: &TabUuid,
        data: &serde_json::Value,
    ) -> sqlx::Result<()> {
        let mut sqlite_conn_guard = self.sqlite_conn.lock().await;

        sqlx::query(
            "
            INSERT INTO crawl_data (tab_uuid, data) VALUES(?1, ?2)
            ON CONFLICT(tab_uuid)
            DO UPDATE SET data=json_patch(data, ?2)
            ",
        )
        .bind(tab_uuid.to_string())
        .bind(data.to_string())
        .execute(&mut *sqlite_conn_guard)
        .await
        .map(|_| ())
    }

    /// Record timing data about the page.
    ///
    /// The provided [`PageTiming`] data will be stored as JSON, with its [`PageTiming::duration`]
    /// field represented as milliseconds and limited to values that can be stored in a [`u64`].
    ///
    /// Use [`DataManager::record_tab()`] to associate this data to a tuple (`crawl_url`, `url`,
    /// `ranking`, `landing_page`); [`DataManager::record_tab()`] does not necessarily have to be
    /// called before this function.
    ///
    /// # Errors
    /// Will return [`sqlx::Error`] if an error happens when upserting page load time.
    pub async fn record_page_timing(
        &self,
        tab_uuid: &TabUuid,
        page_timimg: &PageTiming,
    ) -> sqlx::Result<()> {
        let mut sqlite_conn_guard = self.sqlite_conn.lock().await;

        sqlx::query(
            "
            INSERT INTO crawl_data (tab_uuid, page_timings) VALUES(?1, ?2)
            ON CONFLICT(tab_uuid)
            DO UPDATE SET page_timings=json_patch(page_timings, ?2)
            ",
        )
        .bind(tab_uuid.to_string())
        .bind(page_timimg.to_string())
        .execute(&mut *sqlite_conn_guard)
        .await
        .map(|_| ())
    }

    /// Record filepaths of screenshots taken of a page.
    ///
    /// `screenshot_paths` must be a JSON map, it will be merged with already stored screenshot
    /// filepaths, allowing to store multiple filepaths.
    ///
    /// Use [`DataManager::record_tab()`] to associate this data to a tuple (`crawl_url`, `url`,
    /// `ranking`, `landing_page`); [`DataManager::record_tab()`] does not necessarily have to be
    /// called before this function.
    ///
    /// # Errors
    /// Will return [`sqlx::Error`] if an error happens when upserting the screenshot path.
    pub async fn record_screenshot_paths(
        &self,
        tab_uuid: &TabUuid,
        screenshot_paths: &serde_json::Value,
    ) -> sqlx::Result<()> {
        let mut sqlite_conn_guard = self.sqlite_conn.lock().await;

        sqlx::query(
            "
            INSERT INTO crawl_data (tab_uuid, screenshot_paths) VALUES(?1, ?2)
            ON CONFLICT(tab_uuid)
            DO UPDATE SET screenshot_paths=json_patch(screenshot_paths, ?2)
            ",
        )
        .bind(tab_uuid.to_string())
        .bind(screenshot_paths.to_string())
        .execute(&mut *sqlite_conn_guard)
        .await
        .map(|_| ())
    }

    /// Record an error encountered for a page.
    ///
    /// Use [`DataManager::record_tab()`] to associate this data to a tuple (`crawl_url`, `url`,
    /// `ranking`, `landing_page`); [`DataManager::record_tab()`] does not necessarily have to be
    /// called before this function.
    ///
    /// # Errors
    /// Will return [`sqlx::Error`] if an error happens when upserting the error.
    pub async fn record_error(&self, crawl_url: &str, error: &str) -> sqlx::Result<()> {
        let mut sqlite_conn_guard = self.sqlite_conn.lock().await;

        sqlx::query(
            "
            INSERT INTO crawl_data (crawl_url, error) VALUES(?1, ?2)
            ",
        )
        .bind(crawl_url)
        .bind(error)
        .execute(&mut *sqlite_conn_guard)
        .await
        .map(|_| ())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use serial_test::serial;
    use sqlx::sqlite::SqliteRow;
    use sqlx::{Column, Row};

    async fn get_all_rows(
        db_conn: &tokio::sync::Mutex<SqliteConnection>,
    ) -> Vec<sqlx::sqlite::SqliteRow> {
        let mut db_conn_guard = db_conn.lock().await;

        sqlx::query(
            "
            SELECT * FROM crawl_data
            ",
        )
        .fetch_all(&mut *db_conn_guard)
        .await
        .unwrap()
    }

    fn assert_db_columns(row: &SqliteRow) {
        assert_eq!(
            row.columns()
                .iter()
                .map(|col| col.name())
                .collect::<Vec<_>>(),
            vec![
                "tab_uuid",
                "crawl_url",
                "url",
                "ranking",
                "landing_page",
                "crawl_type",
                "page_timings",
                "screenshot_paths",
                "data",
                "error"
            ],
        );
    }

    #[tokio::test]
    #[serial]
    async fn test_data_manager_data_first() {
        let tmp_dir = tempdir::TempDir::new("").unwrap();
        std::env::set_current_dir(&tmp_dir).unwrap();

        let data_manager = DataManager::new("crawl_data.db").await.unwrap();

        let tab_uuid = TabUuid("6c3975e6-e72d-11ec-9c55-47e28fe513de".to_string());

        // Insert new data
        data_manager
            .record_data(&tab_uuid, &serde_json::from_str(r#"{"a": 0}"#).unwrap())
            .await
            .unwrap();

        let rows = get_all_rows(&data_manager.sqlite_conn).await;
        assert_eq!(rows.len(), 1);
        assert_eq!(
            rows[0].get::<String, &str>("tab_uuid"),
            tab_uuid.to_string()
        );
        assert_eq!(rows[0].get::<String, &str>("crawl_url"), String::new());
        assert_eq!(rows[0].get::<String, &str>("url"), String::new());
        assert_eq!(rows[0].get::<String, &str>("data"), r#"{"a":0}"#);

        // Merge new data into existing data
        data_manager
            .record_data(&tab_uuid, &serde_json::from_str(r#"{"b": 1}"#).unwrap())
            .await
            .unwrap();

        let rows = get_all_rows(&data_manager.sqlite_conn).await;
        assert_eq!(rows.len(), 1);
        assert_db_columns(&rows[0]);
        assert_eq!(
            rows[0].get::<String, &str>("tab_uuid"),
            tab_uuid.to_string()
        );
        assert_eq!(rows[0].get::<String, &str>("crawl_url"), String::new());
        assert_eq!(rows[0].get::<String, &str>("url"), String::new());
        assert_eq!(rows[0].get::<String, &str>("crawl_type"), String::new());
        assert_eq!(rows[0].get::<String, &str>("page_timings"), "{}");
        assert_eq!(rows[0].get::<String, &str>("screenshot_paths"), "{}");
        assert_eq!(rows[0].get::<String, &str>("data"), r#"{"a":0,"b":1}"#);

        data_manager
            .record_tab(
                "https://rust-lang.org",
                "https://rust-lang.org/index.html",
                42,
                true,
                &tab_uuid,
                "js",
            )
            .await
            .unwrap();

        let rows = get_all_rows(&data_manager.sqlite_conn).await;
        assert_eq!(rows.len(), 1);
        assert_eq!(
            rows[0].get::<String, &str>("tab_uuid"),
            tab_uuid.to_string(),
        );
        assert_eq!(
            rows[0].get::<String, &str>("crawl_url"),
            "https://rust-lang.org",
        );
        assert_eq!(
            rows[0].get::<String, &str>("url"),
            "https://rust-lang.org/index.html",
        );
        assert_eq!(rows[0].get::<String, &str>("crawl_type"), "js");
        assert_eq!(rows[0].get::<String, &str>("data"), r#"{"a":0,"b":1}"#);

        data_manager
            .record_page_timing(
                &tab_uuid,
                &PageTiming {
                    name: "page_load".to_string(),
                    duration: Duration::from_millis(4321),
                },
            )
            .await
            .unwrap();

        let rows = get_all_rows(&data_manager.sqlite_conn).await;
        assert_eq!(
            rows[0].get::<String, &str>("page_timings"),
            r#"{"page_load":4321}"#
        );

        data_manager
            .record_screenshot_paths(&tab_uuid, &json!({ "main": "./screenshot.png" }))
            .await
            .unwrap();

        let rows = get_all_rows(&data_manager.sqlite_conn).await;
        assert_eq!(
            rows[0].get::<String, &str>("screenshot_paths"),
            r#"{"main":"./screenshot.png"}"#
        );

        // Test adding a new path
        data_manager
            .record_screenshot_paths(&tab_uuid, &json!({ "second": "./screenshot2.png" }))
            .await
            .unwrap();

        let rows = get_all_rows(&data_manager.sqlite_conn).await;
        assert_eq!(
            rows[0].get::<String, &str>("screenshot_paths"),
            r#"{"main":"./screenshot.png","second":"./screenshot2.png"}"#
        );
    }

    #[tokio::test]
    #[serial]
    async fn test_data_manager_tab_first() {
        let tmp_dir = tempdir::TempDir::new("").unwrap();
        std::env::set_current_dir(&tmp_dir).unwrap();

        let data_manager = DataManager::new("crawl_data.db").await.unwrap();

        let tab_uuid = TabUuid("6c3975e6-e72d-11ec-9c55-47e28fe513de".to_string());

        // Record the tab information
        data_manager
            .record_tab(
                "https://rust-lang.org",
                "https://rust-lang.org/index.html",
                42,
                true,
                &tab_uuid,
                "js",
            )
            .await
            .unwrap();

        // Insert new data
        data_manager
            .record_data(&tab_uuid, &serde_json::from_str(r#"{"a": 0}"#).unwrap())
            .await
            .unwrap();
        data_manager
            .record_data(&tab_uuid, &serde_json::from_str(r#"{"b": 1}"#).unwrap())
            .await
            .unwrap();

        let rows = get_all_rows(&data_manager.sqlite_conn).await;
        assert_eq!(rows.len(), 1);
        assert_eq!(
            rows[0].get::<String, &str>("tab_uuid"),
            tab_uuid.to_string()
        );
        assert_eq!(
            rows[0].get::<String, &str>("crawl_url"),
            "https://rust-lang.org"
        );
        assert_eq!(
            rows[0].get::<String, &str>("url"),
            "https://rust-lang.org/index.html"
        );
        assert_eq!(rows[0].get::<String, &str>("data"), r#"{"a":0,"b":1}"#);
    }

    #[tokio::test]
    #[serial]
    async fn test_recording_error() {
        let tmp_dir = tempdir::TempDir::new("").unwrap();
        std::env::set_current_dir(&tmp_dir).unwrap();

        let data_manager = DataManager::new("crawl_data.db").await.unwrap();

        data_manager
            .record_error("https://rust-lang.org", "error in page")
            .await
            .unwrap();

        let rows = get_all_rows(&data_manager.sqlite_conn).await;
        assert_eq!(rows.len(), 1);
        assert_eq!(rows[0].get::<String, &str>("error"), "error in page");

        data_manager
            .record_error("https://rust-lang.org", "error in page")
            .await
            .unwrap();

        let rows = get_all_rows(&data_manager.sqlite_conn).await;
        assert_eq!(rows.len(), 2);
        assert_eq!(rows[0].get::<String, &str>("error"), "error in page");
    }

    #[tokio::test]
    #[serial]
    async fn test_multiple_page_timings() {
        let tmp_dir = tempdir::TempDir::new("").unwrap();
        std::env::set_current_dir(&tmp_dir).unwrap();

        let data_manager = DataManager::new("crawl_data.db").await.unwrap();

        let tab_uuid = TabUuid("6c3975e6-e72d-11ec-9c55-47e28fe513de".to_string());

        data_manager
            .record_page_timing(
                &tab_uuid,
                &PageTiming {
                    name: "load_time".to_string(),
                    duration: Duration::from_millis(4321),
                },
            )
            .await
            .unwrap();

        data_manager
            .record_page_timing(
                &tab_uuid,
                &PageTiming {
                    name: "post_load_time".to_string(),
                    duration: Duration::from_millis(1234),
                },
            )
            .await
            .unwrap();

        let rows = get_all_rows(&data_manager.sqlite_conn).await;
        assert_eq!(rows.len(), 1);
        assert_eq!(
            rows[0].get::<String, &str>("page_timings"),
            r#"{"load_time":4321,"post_load_time":1234}"#
        );
    }
}
