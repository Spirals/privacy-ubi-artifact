# Artifact for paper "And Nothing of Value Was Lost: Introducing User Browsing Intent for Web Content Blocking"

This repository contains the artifact for this article.

The subdirectories contains the various repositories part of this artifact.
Each of them contains a brief `README.md` describing their role.

The main repositories are the following:
- `ubi-a-webext` contains the repair WebExtension described in the paper.
- `crawl_ubi-a` contains the web crawler described in the paper, along with the data analysis
  Jupyter notebook.
- `browser-screenshot-diff` contains the pixel-to-pixel image comparison tool described in the paper.

## License

Copyright (c) 2023 Inria
