'use strict';

const IDLE_TIMEOUT = 5000;

const inFlightRequests = [];
const timeoutIDs = [];

const filterObjectProps = (obj, propArray) => {
  return Object.fromEntries(Object.entries(obj).filter(([key]) => propArray.includes(key)));
};

const tabRequests = [];

const onBeforeRequest = (details) => {
  const reqDetails = filterObjectProps(details, [
    'documentUrl',
    'originUrl',
    'frameId',
    'thirdParty',
    'method',
    'type',
    'url',
    'urlClassification',
  ]);

  const tabId = details.tabId;
  tabRequests[tabId] = tabRequests[tabId]?.concat([reqDetails]) || [reqDetails];
};

const onRequestSent = (details) => {
  const tabId = details.tabId;

  if (timeoutIDs[tabId]) {
    clearTimeout(timeoutIDs[tabId]);
  }

  if (inFlightRequests[tabId]) {
    inFlightRequests[tabId].add(details.requestId);
  } else {
    inFlightRequests[tabId] = new Set().add(details.requestId);
  }
};

const getTabIdData = async (tabId) => {
  let intervalId;

  const result = await new Promise((resolve) => {
    intervalId = setInterval(
      async () => {
        const tabUuid = await browser.tabs.executeScript(
          tabId,
          { code: 'document.getElementById("tab-uuid")?.textContent' },
        ).then(
          (frameResults) => frameResults[0],
          () => resolve(),
        );

        const crawlType = await browser.tabs.executeScript(
          tabId,
          { code: 'document.getElementById("crawl-type")?.textContent' },
        ).then(
          (frameResults) => frameResults[0],
          () => resolve(),
        );

        if (tabUuid && crawlType) {
          resolve({ tabUuid, crawlType });
        }
      },
      50,
    );
  });

  clearInterval(intervalId);

  return {
    tabUuid: result?.tabUuid,
    crawlType: result?.crawlType,
  };
};

const onRequestFinished = (details) => {
  const tabId = details.tabId;

  inFlightRequests[tabId].delete(details.requestId);

  clearTimeout(timeoutIDs[tabId]);

  const inFlightRequestCount = inFlightRequests[tabId].size;
  if (inFlightRequestCount > 0) {
    return;
  }

  // If the tab has been closed before the request finished
  if (tabId < 0) {
    return;
  }

  timeoutIDs[tabId] = setTimeout(
    async () => {
      // Avoid moz-extension:// background pages
      if (details.documentUrl && !details.documentUrl.startsWith('http')) {
        return;
      }

      console.log(`Network idle for ${IDLE_TIMEOUT}\xa0ms on ${details.documentUrl}`);

      const { tabUuid, crawlType } = await getTabIdData(tabId);

      if (!tabUuid || !crawlType) {
        return;
      }

      const urlSearchParams = new URLSearchParams();
      urlSearchParams.set('url', details.documentUrl);
      urlSearchParams.set('tabUuid', tabUuid);
      urlSearchParams.set('crawlType', crawlType);
      urlSearchParams.set('requestList', JSON.stringify(tabRequests[tabId]));

      await fetch('http://127.0.0.1:3000/networkidle', {
        method: 'POST',
        body: urlSearchParams,
      });
    },
    IDLE_TIMEOUT,
  );
};

browser.webRequest.onBeforeRequest.addListener(
  onBeforeRequest,
  { urls: ['<all_urls>'] },
);

browser.webRequest.onSendHeaders.addListener(
  onRequestSent,
  { urls: ['<all_urls>'] },
);

browser.webRequest.onCompleted.addListener(
  onRequestFinished,
  { urls: ['<all_urls>'] },
);

browser.webRequest.onErrorOccurred.addListener(
  onRequestFinished,
  { urls: ['<all_urls>'] },
);
