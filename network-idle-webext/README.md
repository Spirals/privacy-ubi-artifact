# `network-idle-webext`

This WebExtension is used in the crawl.
It notifies the crawler when the network of a tab is idle, because the WebDriver API does not allow
to do this.

## License

Licensed under the Apache-2.0 license (see LICENSE).

Copyright (c) 2022 Inria
