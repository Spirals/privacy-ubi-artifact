# js-blocker-webext``

This WebExtension is used in the crawl to block JavaScript by injecting a CSP header.

## License

Licensed under the Apache-2.0 license (see LICENSE).

Copyright (c) 2022 Inria
