'use strict';

const isAboutSizeOf = (lengthA, lengthB, tolerance) => {
  if (tolerance.unit === 'px') {
    return Math.abs(lengthB - lengthA) <= tolerance.value;
  }

  return false;
};

const hidePreloaders = async () => {
  const preloaderPatternsJSON
    = await (await fetch(browser.runtime.getURL('src/assets/preloader-patterns.json')))
      .text();
  const preloaderPatterns = JSON.parse(preloaderPatternsJSON);

  let hiddenPreloaderCount = 0;

  for (const pattern of preloaderPatterns.preloaders) {
    const elements = document.querySelectorAll(pattern.target);

    elements.forEach((el) => {
      if (!pattern.size?.mustCoverViewport
        || isAboutSizeOf(el.clientHeight, window.innerHeight, pattern.size.tolerance)
          && isAboutSizeOf(el.clientWidth, document.body.clientWidth, pattern.size.tolerance)) {
        // !important is needed on some sites, e.g. https://web.archive.org/web/20210525040603/https://quantumwarp.com/
        el.style.setProperty('display', 'none', 'important');
        hiddenPreloaderCount++;
      }
    });
  }

  browser.runtime.sendMessage({
    tabData: true,
    hiddenPreloaderCount,
  });
};

console.log('hide-preloaders');

hidePreloaders();
