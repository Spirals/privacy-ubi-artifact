'use strict';

const isValidSrcValue = (string) => {
  string = string.trim();

  if (string.startsWith('//')) {
    string = `${document.location.protocol}${string}`;
  } else {
    if (string.startsWith('/')) {
      string = `${document.baseURI}${string}`;
    }
  }

  try {
    return ['http:', 'https:', 'data:'].includes(new URL(string).protocol);
  } catch {
    return false;
  }
};

const isValidSrcsetValue = (string) => {
  // https://html.spec.whatwg.org/multipage/images.html#srcset-attribute

  return string.split(/, | ,/).every((s) => {
    s = s.trim();

    // Split on spaces and keep only non-empty strings
    const splitS = s.split(' ').filter((a) => a.length > 0);

    if (splitS.length < 1) {
      return false;
    }

    if (!isValidSrcValue(splitS[0])) {
      return false;
    }

    // The image candidate string may not contain a width descriptor or pixel density descriptor
    if (splitS.length === 1) {
      return true;
    }

    if (splitS.length !== 2) {
      return false;
    }

    const descriptor = splitS[1];

    if (descriptor.at(-1) === 'w') {
      return parseInt(splitS[1].slice(0, -1)) > 0;
    }

    if (descriptor.at(-1) === 'x') {
      return parseFloat(splitS[1].slice(0, -1)) > 0;
    }

    return false;
  });
};

const detectSrcValue = (el, destAttr) => {
  const candidateSrcValues = Array.from(el.attributes)
    .filter(({ name, value }) => {
      // No need to check these standard attributes
      if (['src', 'srcset'].includes(name)) {
        return false;
      }

      if (destAttr === 'src') {
        if (isValidSrcValue(value)) {
          return true;
        }
      }

      if (destAttr === 'srcset') {
        if (isValidSrcsetValue(value)) {
          return true;
        }
      }

      return false;
    })
    .map(({ value }) => value);

  if (candidateSrcValues.length === 0) {
    return null;
  }

  if (destAttr === 'src') {
    return candidateSrcValues[0];
  }

  // If we're looking to populate the srcset attribute, find the first value that
  // looks like a valid srcset value and not simply like valid a src value
  // e.g. the element contains data-src and data-srcset attributes, we want to try and
  // populate the srcset attribute with the data-srcset value not the data-src value
  if (destAttr === 'srcset') {
    const srcsetValue = candidateSrcValues
      .find((value) => isValidSrcsetValue(value) && value.match(/, | ,/));
    if (srcsetValue) {
      return srcsetValue;
    }
  }

  return candidateSrcValues
    .find((value) => isValidSrcValue(value));
};

// TESTING
// https://www.etudiant.gouv.fr/
// https://web.archive.org/web/20220318155428/https://www.pcguide.com/reviews/best-fan-controller/
// https://web.archive.org/web/20220314213253/https://www.mightynetworks.com/
const fixJSOnlyLazyLoadedImages = async () => {
  const lazyLoadedImagePatternsJSON
    = await (await fetch(browser.runtime.getURL('src/assets/lazy-loaded-image-patterns.json')))
      .text();
  const lazyLoadedImagePatterns = JSON.parse(lazyLoadedImagePatternsJSON);

  let lazyLoadedImageCount = 0;

  // Set the src/srcset attributes and remove placeholder filters when the image
  // is loaded if needed
  for (const destAttr of Object.keys(lazyLoadedImagePatterns.imageSrc)) {
    for (const pattern of lazyLoadedImagePatterns.imageSrc[destAttr]) {
      for (const target of pattern.targets) {
        document.querySelectorAll(target).forEach((el) => {
          // Always overwrite the src/srcset attribute even if it is non-empty, since
          // it can be a low-res or 1x1 placeholder image, e.g.
          // https://web.archive.org/web/20211029055239/https://null-byte.wonderhowto.com/how-to/hackers-use-hidden-data-airline-boarding-passes-hack-flights-0180728/

          if (pattern.srcAttr || pattern.autoDetectAttr) {
            let srcValue = el.getAttribute(pattern.srcAttr);

            if (pattern.autoDetectAttr) {
              srcValue = detectSrcValue(el, destAttr);
            }

            if (!srcValue) {
              return;
            }

            el.setAttribute(destAttr, srcValue);

            lazyLoadedImageCount++;
          }

          // When the element is a <source>, make sure to work on the associated
          // <img> instead
          if (el.tagName === 'SOURCE') {
            el = el.parentElement.querySelector('img');
          }

          el.addEventListener('load', () => {
            for (const style of lazyLoadedImagePatterns.mainImagesToShow) {
              const elStyle = window.getComputedStyle(el);
              if (window.utils.testCond(
                elStyle[style.condition.prop],
                style.condition.test,
                style.condition.value,
              )) {
                el.style[style.apply.prop] = style.apply.value;
              }
            }
          });

          // Only lazy-load images below the fold to improve UX
          if (el.getBoundingClientRect().top > window.innerHeight) {
            el.loading = 'lazy';
          }
        });
      }
    }
  }

  // Show the images if they were hidden while loading
  for (const pattern of lazyLoadedImagePatterns.placeholdersToHide) {
    for (const target of pattern.targets) {
      document.querySelectorAll(target).forEach((el) => {
        if (!el.nextElementSibling) {
          return;
        }

        for (const style of pattern.hideStyles) {
          const elStyle = window.getComputedStyle(el);
          if (window.utils.testCond(
            elStyle[style.condition.prop],
            style.condition.test,
            style.condition.value,
          )) {
            const mainImage = el.nextElementSibling.querySelector(pattern.mainImageInNextSibling);
            // Wait for the main image to be loaded before removing the placeholder
            mainImage?.addEventListener('load', () => {
              el.style[style.apply.prop] = style.apply.value;
            });
          }
        }
      });
    }
  }

  browser.runtime.sendMessage({
    tabData: true,
    lazyLoadedImageCount,
  });
};

console.log('noscript-fix-images');

fixJSOnlyLazyLoadedImages();
