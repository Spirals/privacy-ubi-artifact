'use strict';

window.utils = window.utils || {};

window.utils.testCond = (a, stringCond, b, unit) => {
  if (unit) {
    a = a.replace(unit, '');
  }

  if (stringCond === '===') {
    return a === b;
  }

  if (stringCond === '!==') {
    return a !== b;
  }

  if (stringCond === '<=') {
    return parseFloat(a) <= parseFloat(b);
  }

  if (stringCond === '>=') {
    return parseFloat(a) >= parseFloat(b);
  }

  return false;
};
