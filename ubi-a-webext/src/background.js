'use strict';

const copyPropIfNumber = (obj1, obj2, prop) => {
  if (typeof obj2[prop] === 'number') {
    obj1[prop] = obj2[prop];
  }
};

const setURLSearchParams = (urlSearchParams, obj, prop) => {
  urlSearchParams.set(prop, obj[prop]);
};

const getTabIdData = async (tabId) => {
  let intervalId;

  const result = await new Promise((resolve) => {
    intervalId = setInterval(
      async () => {
        const tabUuid = await browser.tabs.executeScript(
          tabId,
          {
            code: 'document.getElementById("tab-uuid")?.textContent',
          },
        ).then(
          (frameResults) => frameResults[0],
          () => resolve(),
        );

        const crawlType = await browser.tabs.executeScript(
          tabId,
          {
            code: 'document.getElementById("crawl-type")?.textContent',
          },
        ).then(
          (frameResults) => frameResults[0],
          () => resolve(),
        );

        if (tabUuid && crawlType) {
          resolve({ tabUuid, crawlType });
        }
      },
      50,
    );
  });

  clearInterval(intervalId);

  return {
    tabUuid: result?.tabUuid,
    crawlType: result?.crawlType,
  };
};

const tabData = [];
const onMessageReceived = async (message, sender) => {
  console.log(message);

  if (message.tabData) {
    const tabId = sender.tab.id;

    const propList = [
      'lazyLoadedImageCount',
      'fadeinElementsShownCount',
      'hiddenPreloaderCount',
      'lazyStylesheetCount',
      'noscriptTagCount',
    ];

    tabData[tabId] = tabData[tabId] || {};
    for (const prop of propList) {
      copyPropIfNumber(tabData[tabId], message, prop);
    }
    console.log(tabId, tabData[tabId]);

    // When we have data about all fixes
    if (Object.values(tabData[tabId]).length === propList.length) {
      const { tabUuid, crawlType } = await getTabIdData(tabId);

      if (!tabUuid || !crawlType) {
        return;
      }

      const urlSearchParams = new URLSearchParams();
      urlSearchParams.set('url', sender.url);
      for (const prop of propList) {
        setURLSearchParams(urlSearchParams, tabData[tabId], prop);
      }
      urlSearchParams.set('tabUuid', tabUuid);
      urlSearchParams.set('crawlType', crawlType);

      await fetch('http://127.0.0.1:3000/fixcount', {
        method: 'POST',
        body: urlSearchParams,
      });
    }
  }

  if (message.popupOpened) {
    browser.runtime.sendMessage({ tabId: message.tabId, data: tabData[message.tabId] });
  }
};

browser.runtime.onMessage.addListener(onMessageReceived);

console.log('background');
