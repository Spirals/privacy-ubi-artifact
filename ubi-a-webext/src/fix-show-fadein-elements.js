'use strict';

// TESTING
// https://web.archive.org/web/20220315080301/https://guaranteedinstallmentloans.com/
// https://web.archive.org/web/20220314213253/https://www.mightynetworks.com/
// https://web.archive.org/web/20220318025253/https://bittrex.com/

const fixFadeinElements = async () => {
  const fadeinElementPatternsJSON
    = await (await fetch(browser.runtime.getURL('src/assets/fadein-element-patterns.json')))
      .text();
  const fadeinElementPatterns = JSON.parse(fadeinElementPatternsJSON);

  let fadeinElementsShownCount = 0;

  for (const pattern of fadeinElementPatterns.fadeinElements) {
    const elements = document.querySelectorAll(pattern.target);

    elements.forEach((el) => {
      const elStyle = window.getComputedStyle(el);

      if (pattern.fadeinStyle.every((condition) => {
        if (condition.prop) {
          const { prop, test, value } = condition;
          return window.utils.testCond(elStyle[prop], test, value);
        }

        return condition.or.some(({ prop, test, value }) => {
          return window.utils.testCond(elStyle[prop], test, value);
        });
      })) {
        console.log(el);

        pattern.showStyle.forEach(({ prop, value, importantFlag }) => {
          el.style.setProperty(prop, value, importantFlag ? 'important' : null);
        });
        fadeinElementsShownCount++;
      }
    });
  }

  browser.runtime.sendMessage({
    tabData: true,
    fadeinElementsShownCount,
  });

};

console.log('fade-in');

fixFadeinElements();
