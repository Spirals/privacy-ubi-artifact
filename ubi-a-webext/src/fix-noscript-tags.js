'use strict';

const parseNoscriptTagContents = (noscriptTag) => {
  // Noscript tags contain only a string, not an actual element tree, so we need to parse that
  // string
  const parser = new DOMParser();
  const fallbackDocument = parser.parseFromString(
    `<span>${noscriptTag.textContent}</span>`,
    'text/html',
  );
  return fallbackDocument.getElementsByTagName('span')[0];
};

const injectNoscriptTag = (noscriptTag) => {
  const fallbackElement = parseNoscriptTagContents(noscriptTag);

  fallbackElement.style.setProperty('display', 'inline', 'important');

  noscriptTag.parentNode.replaceChild(fallbackElement, noscriptTag);
};

const noscriptTagMatches = (noscriptTag, pattern) => {
  const fallbackElement = parseNoscriptTagContents(noscriptTag);

  if (pattern.target) {
    return Boolean(fallbackElement.querySelector(pattern.target));
  }

  if (pattern.textRegexp) {
    const re = new RegExp(pattern.textRegexp, pattern.ignoreCase ? 'i' : '');
    return Boolean(fallbackElement.textContent.match(re));
  }

  return false;
};

const fixNoscriptTags = async () => {
  const noscriptTagPatternsJSON
    = await (await fetch(browser.runtime.getURL('src/assets/noscript-tag-patterns.json')))
      .text();
  const noscriptTagPatterns = JSON.parse(noscriptTagPatternsJSON);

  let noscriptTagCount = 0;

  for (const pattern of noscriptTagPatterns.noscriptTags) {
    document.querySelectorAll(pattern.target).forEach((noscriptTag) => {
      if (pattern.has.every((innerPattern) => noscriptTagMatches(noscriptTag, innerPattern))
        && !pattern.hasNo.some((innerPattern) => noscriptTagMatches(noscriptTag, innerPattern))) {
        injectNoscriptTag(noscriptTag);

        noscriptTagCount++;
      }
    });
  }

  browser.runtime.sendMessage({
    tabData: true,
    noscriptTagCount,
  });
};

console.log('noscript-fix-noscript-tags');

fixNoscriptTags();
