'use strict';

const LINK_ATTRIBUTE_ALLOWLIST = [
  'rel',
  'media',
  'as',
  'disabled',
];

// TESTING
// https://web.archive.org/web/20220320191644/https://www.nature.com/articles/nmeth.1618
// https://web.archive.org/web/20220325032203/http://www.denverpost.com/

const injectLazyStylesheet = (url) => {
  const linkElement = document.createElement('link');
  linkElement.rel = 'stylesheet';
  linkElement.href = url;
  linkElement.referrerpolicy = 'no-referrer';

  // Append the link element at the end of the body, so that it is loaded after the content
  document.body.appendChild(linkElement);
};

const fixLazyStylesheets = async () => {
  const lazyStylesheetPatternsJSON
    = await (await fetch(browser.runtime.getURL('src/assets/lazy-stylesheet-patterns.json')))
      .text();
  const lazyStylesheetPatterns = JSON.parse(lazyStylesheetPatternsJSON);

  let lazyStylesheetCount = 0;

  for (const pattern of lazyStylesheetPatterns.lazyStylesheets) {
    document.querySelectorAll(pattern.target).forEach((linkStylesheet) => {
      pattern.applyToLoad.forEach(({ prop, value }) => {
        if (!LINK_ATTRIBUTE_ALLOWLIST.includes(prop)) {
          return;
        }
        linkStylesheet[prop] = value;
      });

      // DO NOT add the onload attribute in the attribute allowlist, as that would allow arbitrary
      // code execution from the configuration file
      linkStylesheet.onload = null;

      lazyStylesheetCount++;
    });
  }

  for (const pattern of lazyStylesheetPatterns.scriptLoaded) {
    // This is likely vulnerable to regex DoS
    const re = new RegExp(pattern.regexp, 'g');
    document.querySelectorAll(pattern.target).forEach((script) => {
      for (const m of script.textContent.matchAll(re)) {
        const stylesheetUrl = m[1];
        injectLazyStylesheet(stylesheetUrl);
      }
    });
  }

  browser.runtime.sendMessage({
    tabData: true,
    lazyStylesheetCount,
  });
};

console.log('lazy-stylesheets');

fixLazyStylesheets();
