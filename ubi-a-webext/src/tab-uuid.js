'use strict';

const JSON_ID = 'tab-uuid';
let tabUuidElement = document.getElementById(JSON_ID);
let tabUuid;

if (tabUuidElement) {
  tabUuid = tabUuidElement.textContent;
} else {
  tabUuid = crypto.randomUUID();
  tabUuidElement = document.createElement('script');
  tabUuidElement.id = JSON_ID;
  tabUuidElement.type = 'text/plain';
  tabUuidElement.textContent = tabUuid;
  (document.head || document.documentElement).appendChild(tabUuidElement);
}

// Return the UUID to the background script
// eslint-disable-next-line no-unused-expressions
tabUuid;
