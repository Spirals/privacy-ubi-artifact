'use strict';

const updateDisplayedCounts = (elementId, count) => {
  const el = document.getElementById(elementId);
  el.textContent = `${count}`;
};

const getCurrentTab = () => {
  return browser.tabs.query({
    active: true,
    windowId: browser.windows.WINDOW_ID_CURRENT,
  });
};

const isNumber = (x) => {
  return typeof x === 'number';
};

const onMessageReceived = (message) => {
  getCurrentTab().then((tabs) => {
    const tabId = tabs[0].id;

    console.log(message, tabId);

    if (message.tabId !== tabId) {
      return;
    }

    const data = message.data;
    if (!data) {
      return;
    }

    if (isNumber(data.lazyLoadedImageCount)) {
      updateDisplayedCounts('lazy-loaded-image-count', data.lazyLoadedImageCount);
    }

    if (isNumber(data.fadeinElementsShownCount)) {
      updateDisplayedCounts('fadein-element-count', data.fadeinElementsShownCount);
    }

    if (isNumber(data.hiddenPreloaderCount)) {
      updateDisplayedCounts('preloader-count', data.hiddenPreloaderCount);
    }

    if (isNumber(data.lazyLoadedImageCount)) {
      updateDisplayedCounts('lazy-stylesheet-count', data.lazyStylesheetCount);
    }

    if (isNumber(data.noscriptTagCount)) {
      updateDisplayedCounts('noscript-fallback-count', data.noscriptTagCount);
    }
  });
};

browser.runtime.onMessage.addListener(onMessageReceived);

getCurrentTab().then((tabs) => {
  const tabId = tabs[0].id;
  browser.runtime.sendMessage({ popupOpened: true, tabId });
});
