# `ubi-a-webext`

The WebExtension implementing the repairs.

The declarative repair configuration can be found in `src/assets`

## License

Licensed under the Apache-2.0 license (see LICENSE).

Copyright (c) 2022 Inria
