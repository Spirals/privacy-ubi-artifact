use std::collections::HashMap;
use std::convert::Infallible;
use std::env;
use std::fs::{self, File};
use std::io::BufReader;
use std::net::SocketAddr;
use std::path::Path;
use std::process::{self, Command};
use std::sync::Arc;
use std::time::{Duration, Instant};

use crawl_framework::thirtyfour::extensions::firefox::FirefoxTools;
use crawl_framework::thirtyfour::prelude::*;
use crawl_framework::thirtyfour::session::handle::SessionHandle;
use crawl_framework::thirtyfour::WindowHandle;
use hyper::service::{make_service_fn, service_fn};
use hyper::{Body, Method, Request, Response, Server};
use log::{debug, error, info};
use serde::{Deserialize, Serialize};
use serde_json::json;
use sha2::{Digest, Sha256};

use crawl_framework::tab_idle_waiter::{self, TabIdleWaiter};

static MAX_OPEN_TABS: u8 = 4;

static LOAD_TIMEOUT_S: u16 = 60;
static POST_LOAD_TIMEOUT_S: u64 = 30;

static ARTIFACT_DIR: &str = "artifacts";
static WINDOW_SIZE: (u32, u32) = (1280, 800);

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct TestPage {
    #[serde(skip_serializing_if = "Option::is_none")]
    skip: Option<bool>,
    url: String,
    fixes: Vec<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    hide_elements: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    post_load_timeout: Option<serde_json::value::Number>,
    sha256: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    comments: Option<String>,
}

async fn check_page(
    session: &tokio::sync::Mutex<SessionHandle>,
    window_handle: WindowHandle,
    tools: &FirefoxTools,
    tab_idle_waiter: &TabIdleWaiter,
    config: &TestPage,
) -> WebDriverResult<(bool, TestPage)> {
    let start_timer = Instant::now();

    {
        let session_guard = session.lock().await;
        session_guard
            .switch_to()
            .window(window_handle.clone())
            .await
            .unwrap();
        session_guard.get(&config.url).await?;
    }
    info!(
        "Page {} loaded in {} ms.",
        config.url,
        start_timer.elapsed().as_millis()
    );

    crawl_framework::scroll_page(session, window_handle.clone())
        .await
        .unwrap();

    let post_load_timeout = match config.post_load_timeout {
        Some(ref t) => t.as_u64().unwrap(),
        _ => POST_LOAD_TIMEOUT_S,
    };
    info!(
        "Waiting for up to {} s after load on {}",
        post_load_timeout, config.url
    );
    let post_load_timer = Instant::now();

    tab_idle_waiter
        .wait_for_tab_idle_with_timeout(
            session,
            &window_handle,
            Duration::from_secs(post_load_timeout),
        )
        .await
        .unwrap();
    info!(
        "Waited for {} ms after load on {}",
        post_load_timer.elapsed().as_millis(),
        config.url
    );

    if let Some(ref hide_element_selector) = config.hide_elements {
        let element_hidden_count: serde_json::value::Number;
        {
            let session_guard = session.lock().await;
            session_guard
                .switch_to()
                .window(window_handle.clone())
                .await
                .unwrap();
            element_hidden_count = session_guard
                .execute_script(
                    r#"
                const selector = arguments[0];
                const elementsToHide = document.querySelectorAll(selector);
                elementsToHide.forEach((el) => el.style.opacity = 0);
                return elementsToHide.length;
            "#,
                    vec![json!(hide_element_selector)],
                )
                .await?
                .convert()?;
        }
        info!(
            "{} elements matching {} hidden on {}",
            element_hidden_count, hide_element_selector, config.url,
        );
    } else {
        info!("No elements to hide for {}", config.url);
    }

    let screenshot_path = Path::new(ARTIFACT_DIR).join(format!(
        "screenshot_{}.png",
        crawl_framework::append_timestamp(&crawl_framework::escape_ext4_filepath(&config.url))
    ));
    let png_screenshot =
        crawl_framework::save_page_screenshot(session, window_handle, tools, screenshot_path)
            .await
            .unwrap();
    debug!("Screenshot taken for {}", config.url);

    let mut hasher = Sha256::new();
    hasher.update(png_screenshot);
    let screenshot_hash = hex::encode(hasher.finalize());

    let mut screenshot_matches = true;

    if screenshot_hash == config.sha256 {
        info!(
            "Screenshot hashes of {} match ({})",
            config.url, config.sha256
        );
    } else {
        screenshot_matches = false;
        error!(
            "Screenshot hashes of {} do NOT match (expected {}, got {})",
            config.url, config.sha256, screenshot_hash
        );
    }

    Ok((
        screenshot_matches,
        TestPage {
            sha256: screenshot_hash,
            ..config.clone()
        },
    ))
}

async fn route(
    net_idle_sender: crossbeam_channel::Sender<tab_idle_waiter::TabNetworkIdle>,
    req: Request<Body>,
) -> Result<Response<Body>, Infallible> {
    match (req.method(), req.uri().path()) {
        (&Method::POST, "/fixcount") => {
            let params =
                url::form_urlencoded::parse(hyper::body::to_bytes(req).await.unwrap().as_ref())
                    .into_owned()
                    .collect::<HashMap<String, String>>();
            debug!("Received POST request from ubi-a-web-ext: {params:?}");
            Ok(Response::default())
        }
        (&Method::POST, "/networkidle") => {
            let params =
                url::form_urlencoded::parse(hyper::body::to_bytes(req).await.unwrap().as_ref())
                    .into_owned()
                    .collect::<HashMap<String, String>>();
            debug!("Received POST request from network-idle-web-ext: {params:?}");
            net_idle_sender
                .send(tab_idle_waiter::TabNetworkIdle(tab_idle_waiter::TabUuid(
                    params.get("tabUuid").unwrap().to_string(),
                )))
                .unwrap();
            Ok(Response::default())
        }
        _ => unreachable!(),
    }
}

#[tokio::main(flavor = "current_thread")]
async fn main() -> WebDriverResult<()> {
    env_logger::init();

    let ci_snaphot_file = File::open(env::args().nth(1).unwrap())?;
    let ci_snaphot_reader = BufReader::new(ci_snaphot_file);
    let ci_snapshots: Vec<TestPage> = serde_json::from_reader(ci_snaphot_reader)?;

    debug!("{:?}", ci_snapshots);

    let mut caps = DesiredCapabilities::firefox();
    caps.add_firefox_option("args", ["--headless"])?;

    let mut geckodriver = Command::new("./geckodriver").spawn().unwrap();

    let driver = WebDriver::new("http://localhost:4444", caps).await?;
    driver
        .set_page_load_timeout(Duration::from_secs(LOAD_TIMEOUT_S as u64))
        .await?;

    driver
        .set_window_rect(0, 0, WINDOW_SIZE.0, WINDOW_SIZE.1)
        .await?;
    info!("Set the window size to {}x{}", WINDOW_SIZE.0, WINDOW_SIZE.1);

    // No mutex is needed around tools as it is only used to take screenshots, and cannot be used
    // to switch the current window/tab
    let tools = FirefoxTools::new(driver.handle.clone());
    tools
        .install_addon(
            &fs::canonicalize("./tab-id-webext")
                .unwrap()
                .to_string_lossy(),
            Some(true), // Load the extension as a temporary extension
        )
        .await?;
    tools
        .install_addon(
            &fs::canonicalize("./network-idle-webext")
                .unwrap()
                .to_string_lossy(),
            Some(true), // Load the extension as a temporary extension
        )
        .await?;
    tools
        .install_addon(
            &fs::canonicalize("./js-blocker-webext")
                .unwrap()
                .to_string_lossy(),
            Some(true), // Load the extension as a temporary extension
        )
        .await?;
    tools
        .install_addon(
            &fs::canonicalize("..").unwrap().to_string_lossy(),
            Some(true), // Load the extension as a temporary extension
        )
        .await?;

    let session = Arc::new(tokio::sync::Mutex::new(driver.handle.clone()));

    let (net_idle_sender, net_idle_receiver) = crossbeam_channel::unbounded();

    let tab_idle_waiter = Arc::new(TabIdleWaiter::new(net_idle_receiver.clone()));

    let addr = SocketAddr::from(([127, 0, 0, 1], 3000));
    let new_service = make_service_fn(move |_conn| {
        let sender = net_idle_sender.clone();
        async {
            Ok::<_, Infallible>(service_fn(move |req: Request<Body>| {
                route(sender.clone(), req)
            }))
        }
    });
    let server = Server::bind(&addr).serve(new_service);
    tokio::spawn(server);

    struct TestingOutput {
        all_same_screenshots: bool,
        output: Vec<TestPage>,
    }

    let testing_output = Arc::new(tokio::sync::Mutex::new(TestingOutput {
        all_same_screenshots: true,
        output: vec![],
    }));

    let mut tasks = vec![];
    let tab_count = Arc::new(tokio::sync::RwLock::new(0));

    for config in ci_snapshots {
        debug!("{:?}", config);

        if config.skip == Some(true) {
            info!("Skipping {}", config.url);
            continue;
        }

        // Enforce a maximum open tab count limit
        loop {
            let tab_count_guard = tab_count.read().await;
            if *tab_count_guard < MAX_OPEN_TABS {
                break;
            }
            // Avoid busy waiting
            tokio::time::sleep(Duration::from_millis(10)).await;
        }

        let session_handle = Arc::clone(&session);
        let tab_count_handle = Arc::clone(&tab_count);

        let tools_handle = tools.clone();

        let testing_output_handle = Arc::clone(&testing_output);
        let tab_idle_waiter_handle = Arc::clone(&tab_idle_waiter);

        let new_tab_handle = crawl_framework::open_new_tab(&session_handle)
            .await
            .expect("could not open a new tab");
        let mut tab_count_guard = tab_count.write().await;
        *tab_count_guard += 1;

        tasks.push(tokio::spawn(async move {
            let (screenshot_matches, test_page) = check_page(
                &session_handle,
                new_tab_handle.clone(),
                &tools_handle,
                &tab_idle_waiter_handle,
                &config,
            )
            .await
            .unwrap();

            {
                let mut testing_output_guard = testing_output_handle.lock().await;
                if !screenshot_matches {
                    testing_output_guard.all_same_screenshots = false;
                }
                testing_output_guard.output.push(test_page);
            }

            crawl_framework::close_tab(&session_handle, new_tab_handle)
                .await
                .unwrap();

            let mut tab_count_guard = tab_count_handle.write().await;
            *tab_count_guard -= 1;
        }));
    }

    futures::future::join_all(tasks).await;
    let testing_output_guard = testing_output.lock().await;

    let output_json = serde_json::to_string_pretty(&testing_output_guard.output)?;
    info!("{}", output_json);

    let output_json_path = Path::new(ARTIFACT_DIR).join("screenshot_hashes.json");
    fs::write(output_json_path, output_json)?;

    driver.quit().await?;
    geckodriver.kill().unwrap();

    if !testing_output_guard.all_same_screenshots {
        process::exit(1);
    }

    Ok(())
}
