# `browser-screenshot-diff`

Compute pixel-to-pixel difference between two images.
Can generate a color-coded difference image and difference statistics in JSON format.

Help can be obtained with:
```
cargo run -- --help
```

An example can be found in the `Dockerfile`.

## License

Licensed under the Apache-2.0 license (see LICENSE).

Copyright (c) 2022 Inria
