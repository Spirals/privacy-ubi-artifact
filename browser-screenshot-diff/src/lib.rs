use std::path::{Path, PathBuf};
use std::time::Instant;

use serde::Serialize;

use image::io::Reader as ImageReader;
use image::{DynamicImage, GenericImageView, ImageBuffer, ImageFormat, Pixel, Rgb, RgbImage};

#[derive(Serialize)]
pub struct ImageDiffStats {
    img_file0: PathBuf,
    img_file1: PathBuf,
    height0: u32,
    height1: u32,
    diff_pixels: u32,
    diff_pixels_perc: f32,
    total_pixels: u32,
    diff_pixels_above_waterline: u32,
    diff_pixels_above_waterline_perc: f32,
    total_pixels_above_waterline_n: u32,
    diff_pixels_below_waterline: u32,
    diff_pixels_below_waterline_perc: f32,
    total_pixels_below_waterline_n: u32,
    likely_viewport_overlay: bool,
    same_height: bool,
    same_width: bool,
    output_img_file: Option<PathBuf>,
}

impl ImageDiffStats {
    pub fn set_output_img_file(&mut self, output_img_file: &Option<PathBuf>) {
        self.output_img_file = output_img_file.clone();
    }
}

fn detect_vertically_shifted_strips(
    img0: &DynamicImage,
    img1: &DynamicImage,
    strip_height: usize,
) -> Vec<usize> {
    let img0_rows = img0.to_rgb8();
    let img0_rows = img0_rows.rows().collect::<Vec<_>>();
    let img0_strips = img0_rows.chunks_exact(strip_height);

    let img1_rows = img1.to_rgb8();
    let img1_rows = img1_rows.rows();

    let mut pixel_strips_found_below = vec![];

    // Split `img0` in `strip_height`-high pixel strips and compare them to `img1` strips
    for (strip_index, img0_strip) in img0_strips.enumerate() {
        let mut same_strip_found = false;

        // Compare each `img0` strip to every possible `strip_height`-high pixel strip from `img1`
        for i in 0..strip_height {
            // Skip the first `i` lines of `img1` before chopping it in strips
            let img1_strips = img1_rows.clone().skip(i).collect::<Vec<_>>();
            let img1_strips = img1_strips.chunks_exact(strip_height);

            let img1_strip_count = img1_strips.len();

            // Start the search at the same strip index (may be faster when content has scrolled
            // down)
            let cyclic_img1_strips = img1_strips.cycle().skip(strip_index).take(img1_strip_count);

            for img1_strip in cyclic_img1_strips {
                // Check every row of the strip
                let all_rows_eq = (0..strip_height).all(|j| {
                    img1_strip[j]
                        .clone()
                        .into_iter()
                        .eq(img0_strip[j].clone().into_iter())
                });

                if all_rows_eq {
                    pixel_strips_found_below.push(strip_index);
                    same_strip_found = true;
                    break;
                }
            }

            if same_strip_found {
                break;
            }
        }
    }

    pixel_strips_found_below
}

pub fn diff_images(
    img_file0: &Path,
    img_file1: &Path,
    viewport_height: u16,
    no_img_output: bool,
    no_vert_shift_detection: bool,
    print_timing_stats: bool,
) -> (ImageBuffer<Rgb<u8>, Vec<u8>>, ImageDiffStats) {
    let now_load = Instant::now();
    let (img0, img1) = rayon::join(
        || {
            let mut reader = ImageReader::open(&img_file0).unwrap_or_else(|err| {
                panic!(
                    "Could not open image {}: {err}",
                    img_file0.to_string_lossy()
                )
            });
            reader.set_format(ImageFormat::Png);
            reader.decode().unwrap_or_else(|_| {
                panic!("Could not decode image {}", img_file0.to_string_lossy())
            })
        },
        || {
            let mut reader = ImageReader::open(&img_file1).unwrap_or_else(|err| {
                panic!(
                    "Could not open image {}: {err}",
                    img_file1.to_string_lossy()
                )
            });
            reader.set_format(ImageFormat::Png);
            reader.decode().unwrap_or_else(|_| {
                panic!("Could not decode image {}", img_file0.to_string_lossy())
            })
        },
    );
    if print_timing_stats {
        println!("Time to load images: {:.2?}", now_load.elapsed());
    }

    let now_init_output = Instant::now();

    let dim0 = img0.dimensions();
    let dim1 = img1.dimensions();

    let (widest_img, narrowest_img) = if dim1.0 > dim0.0 {
        (&img1, &img0)
    } else {
        (&img0, &img1)
    };

    let (_widest_dim, narrowest_dim) = (widest_img.dimensions(), narrowest_img.dimensions());

    let (tallest_img, shortest_img) = if dim1.1 > dim0.1 {
        (&img1, &img0)
    } else {
        (&img0, &img1)
    };

    let (tallest_dim, shortest_dim) = (tallest_img.dimensions(), shortest_img.dimensions());

    let mut diff_output_img = if no_img_output {
        // This helps to save memory since we are not going to use it in this case
        RgbImage::new(0, 0)
    } else {
        RgbImage::new(tallest_dim.0, tallest_dim.1)
    };

    if print_timing_stats {
        println!(
            "Time to initialize output image: {:.2?}",
            now_init_output.elapsed()
        );
    }

    let now_shift_detect = Instant::now();

    let strip_height = 16;

    // Detect vertical shift of `strip_height`-pixel-high strips
    // i.e. detect whether the exact same horizontal pixel strip is found lower in the second image
    let pixel_strips_found_below = if no_vert_shift_detection {
        Vec::new()
    } else {
        detect_vertically_shifted_strips(&img0, &img1, strip_height)
    };

    if print_timing_stats {
        println!(
            "Time to detect vertical shift: {:.2?}",
            now_shift_detect.elapsed()
        );
    }

    let now_diff = Instant::now();

    let mut diff_pixel_counter = 0;
    let mut diff_pixel_above_waterline_counter = 0;
    let mut diff_pixel_below_waterline_counter = 0;

    // Compare every pixel of both images
    for (x, y, pixel_n) in narrowest_img.pixels() {
        if y >= shortest_dim.1 {
            break;
        }

        let pixel_w = widest_img.get_pixel(x, y);

        let pixel_n_luma: f32 = pixel_n.to_luma().channels()[0].into();

        let is_same = pixel_n == pixel_w
            || (!no_vert_shift_detection
                && (pixel_strips_found_below.contains(&(y as usize / strip_height))));

        // Do not consider possible vertical shift for this test
        if pixel_n != pixel_w {
            if y < viewport_height as u32 {
                diff_pixel_above_waterline_counter += 1;
            } else {
                diff_pixel_below_waterline_counter += 1;
            }
        }

        let new_pixel = if is_same {
            let contrast = -25.;
            let percent = ((100. + contrast as f32) / 100.).powi(2);
            // Increase contrast then brighten the pixel
            let new_pixel_luma = (((pixel_n_luma / 255. - 0.5) * percent + 0.5) * 255.) as u8 + 50;
            Rgb::from([new_pixel_luma, new_pixel_luma, new_pixel_luma])
        } else {
            diff_pixel_counter += 1;
            if no_img_output {
                continue;
            }

            let pixel_w_luma = pixel_w.to_luma().channels()[0] as f32;

            let lighter_coeff = match pixel_n_luma as u8 {
                0 => pixel_w_luma / 255.,
                _ => (pixel_w_luma - pixel_n_luma) / pixel_w_luma,
            };

            Rgb::from([
                ((1. - lighter_coeff) * 100_f32) as u8,
                ((1. - lighter_coeff) * 100_f32) as u8,
                (lighter_coeff * 180_f32) as u8,
            ])
        };

        if !no_img_output {
            diff_output_img.put_pixel(x, y, new_pixel);
        }
    }

    let same_height = dim0.1 == dim1.1;

    // If both images do not have the same dimensions
    if !same_height {
        if no_img_output {
            diff_pixel_counter += shortest_dim.0 * (tallest_dim.1 - shortest_dim.1);
            diff_pixel_below_waterline_counter += shortest_dim.0 * (tallest_dim.1 - shortest_dim.1);
        } else {
            let other_pixels = tallest_img
                .pixels()
                .skip(tallest_dim.0 as usize * shortest_dim.1 as usize);

            for (x, y, pixel) in other_pixels {
                let red_coeff = if no_vert_shift_detection
                    || !pixel_strips_found_below.contains(&(y as usize / strip_height))
                {
                    diff_pixel_counter += 1;
                    // Screenshots are at least as tall as the viewport, so all pixels here are
                    // below the waterline
                    diff_pixel_below_waterline_counter += 1;
                    100
                } else {
                    180
                };

                let pixel_luma = pixel.to_luma().channels()[0];
                // Swap the colors if needed
                let pixel_luma: f32 = if dim1.1 > dim0.1 {
                    pixel_luma
                } else {
                    255 - pixel_luma
                }
                .into();

                let new_pixel = Rgb::from([
                    ((1. - pixel_luma / 255.) * red_coeff as f32) as u8,
                    ((1. - pixel_luma / 255.) * 100_f32) as u8,
                    (pixel_luma / 255. * 180_f32) as u8,
                ]);
                diff_output_img.put_pixel(x, y, new_pixel);
            }
        }
    }

    if print_timing_stats {
        println!("Time to diff images: {:.2?}", now_diff.elapsed());
    }

    let total_pixels = narrowest_dim.0 * shortest_dim.1;

    let total_pixels_above_waterline_n = narrowest_dim.0 * viewport_height as u32;
    let total_pixels_below_waterline_n =
        narrowest_dim.0 * (shortest_dim.1 - viewport_height as u32);

    let diff_pixels_perc = 100. * diff_pixel_counter as f32 / total_pixels as f32;
    let diff_pixels_above_waterline_perc =
        100. * diff_pixel_above_waterline_counter as f32 / total_pixels_above_waterline_n as f32;
    let diff_pixels_below_waterline_perc = match total_pixels_below_waterline_n {
        0 => 0.,
        _ => {
            100. * diff_pixel_below_waterline_counter as f32 / total_pixels_below_waterline_n as f32
        }
    };

    let likely_viewport_overlay = diff_pixels_above_waterline_perc > 67.
        && diff_pixels_above_waterline_perc > 1.3 * diff_pixels_below_waterline_perc;

    let same_width = dim0.0 == dim1.0;

    (
        diff_output_img,
        ImageDiffStats {
            img_file0: img_file0.to_path_buf(),
            img_file1: img_file1.to_path_buf(),
            height0: dim0.1,
            height1: dim1.1,
            diff_pixels: diff_pixel_counter,
            diff_pixels_perc,
            total_pixels,
            diff_pixels_above_waterline: diff_pixel_above_waterline_counter,
            diff_pixels_above_waterline_perc,
            total_pixels_above_waterline_n,
            diff_pixels_below_waterline: diff_pixel_below_waterline_counter,
            diff_pixels_below_waterline_perc,
            total_pixels_below_waterline_n,
            likely_viewport_overlay,
            same_height,
            same_width,
            output_img_file: None,
        },
    )
}

#[cfg(test)]
mod tests {
    use super::*;

    use insta::assert_yaml_snapshot;

    // The fixture images are smaller than actual screenshots so that the tests run faster

    #[test]
    fn diff_black_white_100_perc_diff() {
        let s = diff_images(
            Path::new("tests/fixtures/black_192x108.png"),
            Path::new("tests/fixtures/white_192x108.png"),
            108,
            true,
            false,
            false,
        )
        .1;

        assert_eq!(s.diff_pixels_perc, 100.);
        assert_eq!(s.diff_pixels_above_waterline_perc, 100.);
        assert_eq!(s.diff_pixels_below_waterline_perc, 0.);
        assert!(s.likely_viewport_overlay);
        assert!(s.same_height);
        assert_yaml_snapshot!(s);
    }

    #[test]
    fn diff_second_image_wider_and_different() {
        let s = diff_images(
            Path::new("tests/fixtures/black_192x108.png"),
            Path::new("tests/fixtures/white_108x108.png"),
            108,
            true,
            false,
            false,
        )
        .1;

        assert_eq!(s.diff_pixels_perc, 100.);
        assert_eq!(s.diff_pixels_above_waterline_perc, 100.);
        assert_eq!(s.diff_pixels_below_waterline_perc, 0.);
        assert!(s.likely_viewport_overlay);
        assert!(s.same_height);
        assert!(!s.same_width);
        assert_yaml_snapshot!(s);
    }

    #[test]
    fn diff_second_image_taller_and_different() {
        let s = diff_images(
            Path::new("tests/fixtures/white_192x108.png"),
            Path::new("tests/fixtures/blue_noise_on_black_16_aligned_orange_16_strip_in_viewport_192x216.png"),
            108,
            false, // Enable image output
            false,
            false,
        )
        .1;

        assert_eq!(s.diff_pixels_perc, 200.);
        assert_eq!(s.diff_pixels_above_waterline_perc, 100.);
        assert_eq!(s.diff_pixels_below_waterline_perc, 0.);
        assert!(s.likely_viewport_overlay);
        assert!(!s.same_height);
        assert_yaml_snapshot!(s);
    }

    #[test]
    fn diff_aligned_shifted_16px_strip() {
        // The first image has a strip aligned with the strip height
        let s = diff_images(
            Path::new("tests/fixtures/blue_noise_on_black_16_aligned_orange_16_strip_in_viewport_192x216.png"),
            Path::new("tests/fixtures/red_noise_on_white_orange_16_strip_out_of_viewport_192x216.png"),
            108,
            true,
            false,
            false,
        )
        .1;

        assert!(s.diff_pixels_perc < 100.);
        assert_eq!(s.diff_pixels_above_waterline_perc, 100.);
        // This does not take into account possible vertical shift
        assert_eq!(s.diff_pixels_below_waterline_perc, 100.);
        assert!(!s.likely_viewport_overlay);
        assert!(s.same_height);
        assert_yaml_snapshot!(s);
    }

    #[test]
    fn diff_aligned_shifted_16px_strip_no_shift_detection() {
        // The first image has a strip aligned with the strip height
        let s = diff_images(
            Path::new("tests/fixtures/blue_noise_on_black_16_aligned_orange_16_strip_in_viewport_192x216.png"),
            Path::new("tests/fixtures/red_noise_on_white_orange_16_strip_out_of_viewport_192x216.png"),
            108,
            true,
            true,
            false,
        )
        .1;

        assert_eq!(s.diff_pixels_perc, 100.);
        assert_eq!(s.diff_pixels_above_waterline_perc, 100.);
        assert_eq!(s.diff_pixels_below_waterline_perc, 100.);
        assert!(!s.likely_viewport_overlay);
        assert!(s.same_height);
        assert_yaml_snapshot!(s);
    }

    #[test]
    fn diff_unaligned_shifted_16px_strip() {
        // The first image has a strip not aligned with the strip height
        let s = diff_images(
            Path::new("tests/fixtures/blue_noise_on_black_orange_16_strip_in_viewport_192x216.png"),
            Path::new(
                "tests/fixtures/red_noise_on_white_orange_16_strip_out_of_viewport_192x216.png",
            ),
            108,
            true,
            false,
            false,
        )
        .1;

        assert_eq!(s.diff_pixels_perc, 100.);
        assert_eq!(s.diff_pixels_above_waterline_perc, 100.);
        assert_eq!(s.diff_pixels_below_waterline_perc, 100.);
        assert!(!s.likely_viewport_overlay);
        assert!(s.same_height);
        assert_yaml_snapshot!(s);
    }

    #[test]
    fn diff_50_perc_upper_half_diff() {
        let s = diff_images(
            Path::new("tests/fixtures/blue_noise_50_white_50_192x216.png"),
            Path::new("tests/fixtures/white_192x216.png"),
            108,
            true,
            false,
            false,
        )
        .1;

        assert_eq!(s.diff_pixels_perc, 50.);
        assert_eq!(s.diff_pixels_above_waterline_perc, 100.);
        assert_eq!(s.diff_pixels_below_waterline_perc, 0.);
        assert!(s.likely_viewport_overlay);
        assert!(s.same_height);
        assert_yaml_snapshot!(s);
    }

    #[test]
    fn diff_50_perc_lower_half_diff() {
        let s = diff_images(
            Path::new("tests/fixtures/white_50_blue_noise_50_192x216.png"),
            Path::new("tests/fixtures/white_192x216.png"),
            108,
            true,
            false,
            false,
        )
        .1;

        assert_eq!(s.diff_pixels_perc, 50.);
        assert_eq!(s.diff_pixels_above_waterline_perc, 0.);
        assert_eq!(s.diff_pixels_below_waterline_perc, 100.);
        assert!(!s.likely_viewport_overlay);
        assert!(s.same_height);
        assert_yaml_snapshot!(s);
    }

    #[test]
    fn diff_100_perc_lower_half_diff() {
        let s = diff_images(
            Path::new("tests/fixtures/white_50_blue_noise_50_192x216.png"),
            Path::new("tests/fixtures/white_192x108.png"),
            108,
            true,
            false,
            false,
        )
        .1;

        assert_eq!(s.diff_pixels_perc, 100.);
        assert_eq!(s.diff_pixels_above_waterline_perc, 0.);
        assert_eq!(s.diff_pixels_below_waterline_perc, 0.);
        assert!(!s.likely_viewport_overlay);
        assert!(!s.same_height);
        assert_yaml_snapshot!(s);
    }

    #[test]
    fn diff_75_perc_upper_half_diff() {
        let s = diff_images(
            Path::new("tests/fixtures/blue_noise_75_white_25_192x216.png"),
            Path::new("tests/fixtures/white_192x216.png"),
            108,
            true,
            false,
            false,
        )
        .1;

        assert_eq!(s.diff_pixels_perc, 75.);
        assert_eq!(s.diff_pixels_above_waterline_perc, 100.);
        assert_eq!(s.diff_pixels_below_waterline_perc, 50.);
        assert!(s.likely_viewport_overlay);
        assert!(s.same_height);
        assert_yaml_snapshot!(s);
    }

    #[test]
    fn shift_detection_aligned_shifted_16px_strip() {
        // The first image has a strip aligned with the strip height
        let img0 = ImageReader::open(
            "tests/fixtures/blue_noise_on_black_16_aligned_orange_16_strip_in_viewport_192x216.png",
        )
        .expect("")
        .decode()
        .expect("");

        let img1 = ImageReader::open(
            "tests/fixtures/red_noise_on_white_orange_16_strip_out_of_viewport_192x216.png",
        )
        .expect("")
        .decode()
        .expect("");

        let shifted_strip_indices = detect_vertically_shifted_strips(&img0, &img1, 16);

        assert_eq!(shifted_strip_indices, vec![1]);
    }

    #[test]
    fn shift_detection_unaligned_shifted_16px_strip() {
        // The first image has a strip not aligned with the strip height
        let img0 = ImageReader::open(
            "tests/fixtures/blue_noise_on_black_orange_16_strip_in_viewport_192x216.png",
        )
        .expect("")
        .decode()
        .expect("");

        let img1 = ImageReader::open(
            "tests/fixtures/red_noise_on_white_orange_16_strip_out_of_viewport_192x216.png",
        )
        .expect("")
        .decode()
        .expect("");

        let shifted_strip_indices = detect_vertically_shifted_strips(&img0, &img1, 16);

        assert!(shifted_strip_indices.is_empty());
    }
}
