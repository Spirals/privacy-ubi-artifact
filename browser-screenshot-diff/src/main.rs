use std::fs::File;
use std::io::Write;
use std::path::PathBuf;
use std::time::Instant;

use clap::Parser;

use browser_screenshot_diff::diff_images;

#[derive(Parser)]
#[clap(version, about = "Compare two browser screenshots")]
struct Args {
    /// First image
    #[clap(parse(from_os_str))]
    img_file0: PathBuf,

    /// Second image
    #[clap(parse(from_os_str))]
    img_file1: PathBuf,

    /// Viewport height
    #[clap(long)]
    viewport_height: u16,

    /// Output image file
    #[clap(parse(from_os_str), required_unless_present("no-img-output"))]
    output_img_file: Option<PathBuf>,

    /// Disable image output
    #[clap(long)]
    no_img_output: bool,

    /// Disable vertical shift detection
    #[clap(long)]
    no_vert_shift_detection: bool,

    /// Display statistics on differences between images
    #[clap(long)]
    stats: bool,

    /// Save statistics on differences between images to a file with the same filestem as the
    /// output screenshot
    #[clap(long)]
    save_stats: bool,

    /// Display timing statistics
    #[clap(long)]
    timing_stats: bool,
}

fn main() {
    let args = Args::parse();

    let (diff_output_img, mut image_diff_stats) = diff_images(
        &args.img_file0,
        &args.img_file1,
        args.viewport_height,
        args.no_img_output,
        args.no_vert_shift_detection,
        args.timing_stats,
    );

    if !args.no_img_output {
        let now_save = Instant::now();
        let diff_output_img_path = args.output_img_file.as_ref().unwrap();
        diff_output_img
            .save_with_format(diff_output_img_path, image::ImageFormat::Png)
            .unwrap_or_else(|err| {
                panic!(
                    "Could not save diff image to {}: {err}",
                    diff_output_img_path.to_string_lossy()
                )
            });
        image_diff_stats.set_output_img_file(&args.output_img_file);

        if args.timing_stats {
            println!("Time to save output image: {:.2?}", now_save.elapsed());
        }
    }

    if args.stats || args.save_stats {
        let now_json = Instant::now();

        let json_stats =
            serde_json::to_string(&image_diff_stats).expect("could not generate stats JSON");
        if args.stats {
            println!("{}", json_stats);
        }

        if args.save_stats {
            let mut json_stats_file = File::create(
                args.output_img_file
                    .as_ref()
                    .unwrap()
                    .with_extension("json"),
            )
            .expect("could not create the output file.");
            json_stats_file
                .write_all(json_stats.as_bytes())
                .expect("could not write the JSON stats file.");
        }

        if args.timing_stats {
            println!("Time to save JSON stats: {:.2?}", now_json.elapsed());
        }
    }
}
