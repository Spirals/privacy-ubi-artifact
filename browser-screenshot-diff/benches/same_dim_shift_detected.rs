use std::path::PathBuf;

use criterion::{black_box, criterion_group, criterion_main, Criterion};

use browser_screenshot_diff::diff_images;

fn criterion_benchmark(c: &mut Criterion) {
    c.bench_function("diff_images same_dim_shift_detected", |b| b.iter(||
        diff_images(
            PathBuf::from("tests/fixtures/blue_noise_on_black_16_aligned_orange_16_strip_in_viewport_192x216.png"),
            PathBuf::from("tests/fixtures/red_noise_on_white_orange_16_strip_out_of_viewport_192x216.png"),
            1080,
            false,
            false,
            false,
        )
    ));
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
